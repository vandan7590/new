<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('logout',array('as'=>'logout','uses'=>'Login\SessionController@logout'));
Route::get('otp',array('as'=>'otp.view','uses'=>'Login\SessionController@enter_otp'));
Route::post('otp',array('as'=>'otp.store','uses'=>'Login\SessionController@otp_store'));
Route::get('resend/otp',array('as'=>'resend.otp','uses'=>'Login\SessionController@otp_resend'));

Route::group(['middleware' => 'guest'], function(){
    Route::get('login',array('as'=>'login','uses'=>'Login\SessionController@login'));
    Route::post('login',array('as'=>'login.store','uses'=>'Login\SessionController@loginstore'));
    Route::get('signup',array('as'=>'signup.view','uses'=>'Login\SessionController@signup_view'));
    Route::post('signup/store',array('as'=>'signup.store','uses'=>'Login\SessionController@signup_store'));
    Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
    Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');
});

Route::group(['middleware' => 'admin'], function(){
    Route::get('dashboard',array('as'=>'dashboard','uses'=>'Admin\AdminController@dashboard'));
    //Branch Crud 
    Route::get('branch',array('as'=>'branch','uses'=>'Admin\BranchController@branch'));
    Route::post('branch/store',array('as'=>'branch.store','uses'=>'Admin\BranchController@branch_store'));
    Route::get('branch/edit/{id}',array('as'=>'branch.edit','uses'=>'Admin\BranchController@branch_edit'));
    Route::post('branch/edit/{id}',array('as'=>'editbranch.post','uses'=>'Admin\BranchController@branch_editstore'));
    Route::get('branch/delete/{id}',array('as'=>'branch.delete','uses'=>'Admin\BranchController@destroy'));
    //Job Crud
    Route::get('jobtype',array('as'=>'jobtype','uses'=>'Admin\JobtypeController@jobtype'));
    Route::post('jobtype/store',array('as'=>'jobtype.store','uses'=>'Admin\JobtypeController@jobtype_store'));
    Route::get('jobtype/view',array('as'=>'jobtype.view','uses'=>'Admin\JobtypeController@jobtype_view'));
    Route::get('jobtype/edit/{id}',array('as'=>'jobtype.edit','uses'=>'Admin\JobtypeController@edit'));
    Route::post('jobtype/edit/{id}',array('as'=>'editjobtype.post','uses'=>'Admin\JobtypeController@editstore'));
    Route::get('jobtype/delete/{id}',array('as'=>'jobtype.delete','uses'=>'Admin\JobtypeController@destroy'));
    Route::get('employeeview',array('as'=>'employeeview','uses'=>'Admin\RoundController@list_view'));
    //Interviewschedule
    Route::get('round/{id}',array('as'=>'round','uses'=>'Admin\RoundController@view'));
    Route::post('round/{id}',array('as'=>'round.post','uses'=>'Admin\RoundController@round_store'));
    Route::get('round/update/{id}/{status}',array('as'=>'round.edit','uses'=>'Admin\RoundController@status_update'));
    Route::post('round/reschedule_update/{id}',array('as'=>'reschedule.edit','uses'=>'Admin\RoundController@reschedule_update'));
    Route::get('round/update/{id}',array('as'=>'reject','uses'=>'Admin\RoundController@status_reject'));
    Route::get('round/confirm/{id}',array('as'=>'confirm','uses'=>'Admin\RoundController@confirm'));
    Route::get('round/reject/{id}',array('as'=>'rejected','uses'=>'Admin\RoundController@reject'));
    Route::get('round/reschedule/{id}',array('as'=>'reschedule.update','uses'=>'Admin\RoundController@reschedule_confirm'));
    Route::get('round/reschedule/reject/{id}',array('as'=>'reschedule.rejected','uses'=>'Admin\RoundController@reschedule_reject'));
    //Stream Module
    Route::get('stream/add',array('as'=>'stream.add','uses'=>'Admin\StreamController@stream_add'));
    Route::post('stream/add',array('as'=>'stream.post','uses'=>'Admin\StreamController@stream_store'));
    Route::get('stream/edit/{id}',array('as'=>'stream.edit','uses'=>'Admin\StreamController@stream_edit'));
    Route::post('stream/edit/{id}',array('as'=>'editstream.post','uses'=>'Admin\StreamController@stream_editstore'));
    Route::get('stream/delete/{id}',array('as'=>'stream.delete','uses'=>'Admin\StreamController@destroy'));
    //Reference Module
    Route::get('reference/add',array('as'=>'reference.add','uses'=>'Admin\ReferenceController@reference_add'));
    Route::post('reference/add',array('as'=>'reference.post','uses'=>'Admin\ReferenceController@reference_store'));
    Route::get('reference/edit/{id}',array('as'=>'reference.edit','uses'=>'Admin\ReferenceController@reference_edit'));
    Route::post('reference/edit/{id}',array('as'=>'editreference.post','uses'=>'Admin\ReferenceController@reference_editstore'));
    Route::get('reference/delete/{id}',array('as'=>'reference.delete','uses'=>'Admin\ReferenceController@destroy'));
    //Training Module
    Route::get('trainingview',array('as'=>'trainingview','uses'=>'Admin\TrainingController@training_view'));
    Route::post('training/demosessiondate/{id}',array('as'=>'demosessiondate.post','uses'=>'Admin\TrainingController@demo_session'));
    Route::post('training/reschedule/{id}',array('as'=>'demo_reschedule.post','uses'=>'Admin\TrainingController@demo_reschedulerequest'));
    Route::get('training/reschedule/reject/{id}',array('as'=>'reschedule.reject','uses'=>'Admin\TrainingController@reschedule_reject'));
    Route::get('training/approval/{id}',array('as'=>'approval','uses'=>'Admin\TrainingController@approval'));
    Route::get('training/cancel/{id}',array('as'=>'cancel','uses'=>'Admin\TrainingController@cancel'));
    Route::get('document/approval/{id}',array('as'=>'doc.approval','uses'=>'Admin\TrainingController@doc_approval'));
    Route::get('document/cancel/{id}',array('as'=>'doc.cancel','uses'=>'Admin\TrainingController@doc_cancel'));
    Route::get('training/payment/{id}',array('as'=>'payment.view','uses'=>'Admin\TrainingController@view'));
    Route::post('training/charges/{id}',array('as'=>'charges.added','uses'=>'Admin\TrainingController@charges'));
    Route::post('training/payment/{id}',array('as'=>'payment.store','uses'=>'Admin\TrainingController@paymentstore'));
    Route::get('training/print/{id}',array('as'=>'print','uses'=>'Admin\TrainingController@print'));
    Route::get('training/plan',array('as'=>'plan','uses'=>'Admin\PlanController@plan_view'));
    Route::post('training/plan/store',array('as'=>'plan.store','uses'=>'Admin\PlanController@plan_store'));
    Route::get('training/plan/edit/{id}',array('as'=>'plan.edit','uses'=>'Admin\PlanController@edit'));
    Route::post('training/plan/edit/{id}',array('as'=>'plan.editstore','uses'=>'Admin\PlanController@editstore'));
    Route::get('training/plan/delete/{id}',array('as'=>'plan.delete','uses'=>'Admin\PlanController@plan_destroy'));
    //Demosession Module
    Route::get('demosession/view',array('as'=>'demosessionview','uses'=>'Admin\DemosessionController@demosession_view'));
    Route::get('demosession/schedule/{id}',array('as'=>'demosession.schedule','uses'=>'Admin\DemosessionController@demoschedule_view'));
    Route::post('demosession/demodate/{id}',array('as'=>'demodate.post','uses'=>'Admin\DemosessionController@demodate'));
    Route::post('demosession/reschedule/{id}',array('as'=>'rescheduledate.post','uses'=>'Admin\DemosessionController@rescheduledate'));
    Route::get('demosession/reject/{id}',array('as'=>'demosession.reject','uses'=>'Admin\DemosessionController@reschedule_reject'));
    Route::get('demosession/approval/{id}',array('as'=>'visited','uses'=>'Admin\DemosessionController@approval'));
    Route::get('demosession/cancel/{id}',array('as'=>'cancelled','uses'=>'Admin\DemosessionController@cancel'));
    //Exam Module
    Route::get('level/Add',array('as'=>'level.add','uses'=>'Exam\LevelController@level_view'));
    Route::post('level/Add',array('as'=>'level.post','uses'=>'Exam\LevelController@level_store'));
    Route::get('level/edit/{id}',array('as'=>'level.edit','uses'=>'Exam\LevelController@level_edit'));
    Route::post('level/edit/{id}',array('as'=>'editlevel.post','uses'=>'Exam\LevelController@level_editstore'));
    Route::get('level/delete/{id}',array('as'=>'level.delete','uses'=>'Exam\LevelController@destroy'));    
    Route::get('question/add',array('as'=>'question.add','uses'=>'Exam\QuestionController@question'));
    Route::post('question/add',array('as'=>'question.post','uses'=>'Exam\QuestionController@question_add'));
    Route::get('question/edit/{id}',array('as'=>'question.edit','uses'=>'Exam\QuestionController@question_edit'));
    Route::post('question/edit/{id}',array('as'=>'editquestion.post','uses'=>'Exam\QuestionController@question_editstore'));
    Route::get('question/delete/{id}',array('as'=>'question.delete','uses'=>'Exam\QuestionController@question_del'));
    Route::get('exam/status/approval/{id}',array('as'=>'examstatus.approval','uses'=>'Admin\RoundController@exam_approval'));
    Route::get('exam/status/denied/{id}',array('as'=>'examstatus.denied','uses'=>'Admin\RoundController@exam_denied'));
    Route::get('exam/level/{id}/{level}',array('as'=>'level.update','uses'=>'Admin\RoundController@level_update'));
});

Route::group(['middleware' =>'user'], function(){
    //Job Apply
    Route::get('front/jobapply',array('as'=>'jobapply.view','uses'=>'Front\FrontController@front_view'));
    Route::post('front/jobapply',array('as'=>'jobapply.store','uses'=>'Front\FrontController@jobapplication_store'));
    Route::post('front/rescheduleupdate/{id}',array('as'=>'reschedule.store','uses'=>'Front\FrontController@reschedule_store'));
    Route::get('front/confirmation',array('as'=>'confirmation','uses'=>'Front\FrontController@confirmationview'));
    //Training Apply
    Route::get('front/training',array('as'=>'training.view','uses'=>'Front\TrainingController@view'));
    Route::post('front/training',array('as'=>'training.store','uses'=>'Front\TrainingController@store'));
    Route::post('front/training/document',array('as'=>'document.store','uses'=>'Front\TrainingController@document_store'));
    Route::get('front/training/demoreset',array('as'=>'demoreset','uses'=>'Front\TrainingController@reset_demo'));
    Route::post('front/training/reschedule',array('as'=>'reschedulerequest.post','uses'=>'Front\TrainingController@demo_reschedulerequest'));
    Route::post('front/training/payment',array('as'=>'paymentoption.add','uses'=>'Front\TrainingController@payment_option'));
    Route::post('front/training/cashrequest',array('as'=>'cashrequest','uses'=>'Front\TrainingController@cash_request'));
    Route::post('front/training/onlinepayment',array('as'=>'onlinepayment','uses'=>'Front\TrainingController@onlinepayment'));
    Route::post('front/training/query',array('as'=>'payment.query','uses'=>'Front\TrainingController@payment_query'));
    Route::get('front/demo', array('as'=>'front.demo','uses'=>'Front\FrontController@demo'));
    //Demosession Apply
    Route::get('front/demosession',array('as'=>'demosession.view','uses'=>'Front\DemosessionController@demosession_view'));
    Route::post('front/demosession/store',array('as'=>'demosession.store','uses'=>'Front\DemosessionController@demosession_store'));
    Route::post('front/demoreschedule',array('as'=>'demoreschedule.post','uses'=>'Front\DemosessionController@demo_reschedule'));
    Route::get('front/pay',array('as'=>'pay','uses'=>'Front\TrainingController@pay'));
});

Route::get('pay', 'RazorpayController@pay')->name('pay');
// route for make payment request using post method
Route::post('dopayment', 'RazorpayController@dopayment')->name('dopayment');
