<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingappliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingapplies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('stream_id');
            $table->foreign('stream_id')->references('id')->on('streams')->onDelete('cascade');
            $table->unsignedInteger('reference_id');
            $table->foreign('reference_id')->references('id')->on('references')->onDelete('cascade');
            $table->string('resume')->nullable();
            $table->string('demo')->nullable();
            $table->string('demo_date')->nullable();
            $table->string('plan')->nullable();
            $table->string('fees')->nullable();
            $table->string('reschedule_date')->nullable();
            $table->string('request_note')->nullable();
            $table->string('verify')->nullable();
            $table->string('approval')->nullable();
            $table->string('cash_request')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trainingapplies');
    }
}
