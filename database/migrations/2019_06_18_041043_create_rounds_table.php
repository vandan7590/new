<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rounds', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('jobapplication_id');
            $table->foreign('jobapplication_id')->references('id')->on('jobapplications')->onDelete('cascade');
            $table->string('round_type')->nullable();
            $table->string('date_time')->nullable();
            $table->string('notes')->nullable();
            $table->string('status')->nullable();
            $table->string('reschedule_date')->nullable();
            $table->string('request_note')->nullable();
            $table->string('verify')->nullable();
            $table->string('confirm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rounds');
    }
}
