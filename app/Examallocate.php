<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Examallocate extends Model
{
    protected $fillable = [
        'user_id','level_id','result'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function level()
    {
        return $this->belongsTo('App\Level','level_id');
    }
}
