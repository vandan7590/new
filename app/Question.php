<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'level_id','question','marks'
    ];

    public function option()
    {
        return $this->hasMany('App\Option');
    }

    public function level()
    {
        return $this->belongsTo('App\Level','level_id');
    }
}
