<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'user_id','training_id','amount','due_date','payment_option','payment_query'
    ];

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function trainingapplies()
    {
        return $this->belongsTo('App\Trainingapply','training_id');
    }

}
