<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'user_id','doc_type','doc_path','approval'
    ];

    public function users() 
    {
        return $this->belongsTo('App\User','user_id');
    }
}
