<?php

    function set_active($path)
    {
        if($path == '/'){
            return Request::is($path) ? 'm-menu__item--active' : '';
        }else{
            return call_user_func_array('Request::is', (array)$path) ? 'm-menu__item--active' : '';
        }
    }

    function set_active_exp($path)
    {
        if($path == '/'){
            return Request::is($path) ? 'm-menu__item--open m-menu__item--expanded' : '';
        }else{
            return call_user_func_array('Request::is', (array)$path) ? 'm-menu__item--open m-menu__item--expanded' : '';
        }
    }