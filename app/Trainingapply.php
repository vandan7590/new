<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainingapply extends Model
{
    protected $fillable = [
        'user_id','stream_id','reference_id','resume','demo','demo_date','plan','fees','reschedule_date','request_note','verify','approval','cash_request'
    ];

    public function users() 
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }

    public function streams()
    {
        return $this->belongsTo('App\Stream','stream_id');
    }

    public function references()
    {
        return $this->belongsTo('App\Reference','reference_id');
    }

    public static function store($trainingapply, $resume, $plan)
    {
        $filename = str_random(10) .'.'.$resume->getClientOriginalExtension();
        $file = 'resume/'.$filename;
        $filepath = 'resume/';
        $resume->move($filepath,$filename);

        if($plan == "free"){
            $trainingapply = Trainingapply::create([
                'user_id'=>$trainingapply->user_id,
                'stream_id'=>$trainingapply->stream_id,
                'reference_id'=>$trainingapply->reference_id,
                'demo'=>$trainingapply->demo,
                'resume'=>$file,
                'plan'=>$trainingapply->plan,
                'fees'=>'free'
            ]);
        }
        elseif($plan == 'basic'){
            $trainingapply = Trainingapply::create([
                'user_id'=>$trainingapply->user_id,
                'stream_id'=>$trainingapply->stream_id,
                'reference_id'=>$trainingapply->reference_id,            
                'demo'=>$trainingapply->demo,
                'resume'=>$file,
                'plan'=>$trainingapply->plan,
                'fees'=>'12000'
            ]);
        }
        elseif($plan == "professional"){
            $trainingapply = Trainingapply::create([
                'user_id'=>$trainingapply->user_id,
                'stream_id'=>$trainingapply->stream_id,
                'reference_id'=>$trainingapply->reference_id,
                'demo'=>$trainingapply->demo,            
                'resume'=>$file,
                'plan'=>$trainingapply->plan,
                'fees'=>'18000'
            ]);
        }
        elseif($plan == "advanced"){
            $trainingapply = Trainingapply::create([
                'user_id'=>$trainingapply->user_id,
                'stream_id'=>$trainingapply->stream_id,
                'reference_id'=>$trainingapply->reference_id,
                'demo'=>$trainingapply->demo,            
                'resume'=>$file,
                'plan'=>$trainingapply->plan,
                'fees'=>'27000'
            ]);
        }
    }
}
