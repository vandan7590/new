<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $fillable = [
        'user_id','jobapplication_id','round_type','date_time','notes','status','reschedule_date','request_notes','verify','confirm'
    ];

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public static function store($round)
    {
        $round = Round::create([
            'user_id'=>$round->user_id,
            'jobapplication_id'=>$round->jobapplication_id,
            'round_type'=>$round->round_type,
            'date_time'=>$round->date_time,
            'notes'=>$round->notes,
            'status'=>"underprocess",
            'reschedule_date'=>"",
            'request_note'=>"", 
            'confirm'=>"0",
        ]);
    }
}
