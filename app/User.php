<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role','branch_id','name', 'email','facebook_id','password','provider','provider_id','contact_number','app_type','otp','verify'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function loginUser($email,$password)
    {
        return \Auth::attempt(array('email'=>$email,'password'=>$password));
    }

    public function round()
    {
        return $this->hasMany('App\Round');
    }

    public function payment()
    {
        return $this->hasMany('App\Payment');
    }

    public function jobapplications()
    {
        return $this->hasMany('App\Jobapplication');
    }

    public function demosessions()
    {
        return $this->hasMany('App\Demosession');
    }

    public function documents()
    {
        return $this->hasMany('App\Document');
    }

    public function branch()
    {
        return $this->hasMany('App\Branch');
    }

    public function examallocate()
    {
        return $this->hasMany('App\Examallocate');
    }

    public function isAdmin()
    {
        return($this->role=='admin');
    }

    public function isUser()
    {
        return ($this->role=="user");
    }

    public function isApptype()
    {
        return ($this->app_type=="job");
    }

    public function verify()
    {
        return ($this->verify);
    }

    public function branches()
    {
        return $this->belongsTo('App\Branch','branch_id');
    }

    public function otp()
    {
        return $this->otp;
    }

}
