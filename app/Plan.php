<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'plan_name','description','price'
    ];

    public static function store($plan)
    {
        $plan = Plan::create([
            'plan_name'=>$plan->plan_name,
            'description'=>$plan->description,
            'price'=>$plan->price,
        ]);
    }
}
