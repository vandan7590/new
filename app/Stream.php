<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stream extends Model
{
    protected $fillable = [
        'stream_name','description',
    ];

    public function demosessions()
    {
        return $this->hasMany('App\Demosession');
    }
}
