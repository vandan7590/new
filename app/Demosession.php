<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Demosession extends Model
{
    protected $fillable = [
        'user_id','stream_id','reference_id','attend','demodate','reschedule_date','request_note','verify','approval'
    ];

    public static function store($demo)
    {
        $demo = Demosession::create([
            'user_id'=>$demo->user_id,
            'stream_id'=>$demo->stream_id,
            'reference_id'=>$demo->reference_id,
            'attend'=>$demo->attend
        ]);
    }

    public function users() 
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function streams()
    {
        return $this->belongsTo('App\Stream','stream_id');
    }

    public function references()
    {
        return $this->belongsTo('App\Reference','reference_id');
    }
}
