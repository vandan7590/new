<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
    protected $fillable = [
        'reference_name','description',
    ];

    public function demosessions()
    {
        return $this->hasMany('App\Demosession');
    }
}
