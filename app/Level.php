<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = [
        'level','experience','no_of_questions','level_time'
    ];

    public function question()
    {
        return $this->hasMany('App\Question');
    }

    public function examallocate()
    {
        return $this->hasMany('App\Examallocate');
    }

    public static function store($level)
    {
        $level = Level::create([
            'level'=>$level->level,
            'experience'=>$level->experience,
            'no_of_questions'=>$level->no_of_questions,
            'level_time'=>$level->level_time,            
        ]);
    }
}
