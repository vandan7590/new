<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobapplication extends Model
{
    protected $fillable = [
        'user_id','jobtype_id','resume','experience','jointype','status'
    ];

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function jobtypes()
    {
        return $this->belongsTo('App\Jobtype','jobtype_id');
    }

    public static function store($jobapplication,$resume)
    {
        $filename = str_random(10) .'.'.$resume->getClientOriginalExtension();
        $file = 'resume/'.$filename;
        $filepath = 'resume/';
        $resume->move($filepath,$filename);

        $jobapplication = Jobapplication::create([
            'user_id'=>$jobapplication->user_id,
            'jobtype_id'=>$jobapplication->jobtype_id,            
            'resume'=>$file,
            'experience'=>$jobapplication->experience,
            'jointype'=>$jobapplication->jointype,
            'status'=>'0'
        ]);
    }
}
