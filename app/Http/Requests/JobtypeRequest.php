<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobtypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'jobtitle'=>'required|max:50',
            'job_introduction'=>'max:1000',
            'job_description'=>'max:1000'
        ];
    }
}
