<?php

namespace App\Http\Middleware;

use Closure;

class OtpMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(\Auth::user()->otp())
        {            
            return $next($request);
        }
        return redirect()->route('login');
    }
}
