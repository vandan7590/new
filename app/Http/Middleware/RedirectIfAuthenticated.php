<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

    public function __construct(Guard $auth)
    {
       $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->check() && Auth::user()->isAdmin())
		{
            return new RedirectResponse(route('dashboard'));
        }
        elseif($this->auth->check() && \Auth::user()->isUser() && \Auth::user()->isApptype())
        {
            return new RedirectResponse(route('jobapply.view'));
        }
        elseif($this->auth->check() && \Auth::user()->isUser())
        {
            return new RedirectResponse(route('training.view'));
        }
		return $next($request);
    }
}
