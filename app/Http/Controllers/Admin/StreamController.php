<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\StreamRequest;
use Illuminate\Http\Request;
use App\Stream;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class StreamController extends Controller
{
    public function stream_add()
    {                    
        $edit="null";         
        $stream=Stream::all();          
        return view('admin.training.stream',compact('edit','stream'));
    }
    public function stream_store(StreamRequest $request)
    {
        $description=$request->get('description');
        if(empty($description))
        {
            $stream = new Stream(array(
                'stream_name'=>strtoupper($request->get('stream_name')),            
                'description'=>'---',
            ));
            $stream->save();
            flash('Stream Added')->success();            
            return \Redirect::route('stream.add');                        
        }           
        else    
        {            
            $stream=new Stream(array(
                'stream_name'=>$request->get('stream_name'),            
                'description'=>$request->get('description'),
            ));
            $stream->save();
            flash('Stream Added')->success();                        
            return \Redirect::route('stream.add');
        }
    }
    
    public function stream_edit($id)
    {        
        $edit=Stream::where('id','=',$id)->first();
        $stream=Stream::all();
        return view('admin.training.stream',compact('edit','stream'));
    }

    public function stream_editstore(Request $request,$id)
    { 
        $description=$request->get('description');
        if(empty($description)){
            \DB::table('streams')
            ->where('id','=',$id)
            ->update(
                ['stream_name'=>$request->get('stream_name'),
                    'description'=>'---']);
            flash('Stream Updated')->success();                        
            return \Redirect::route('stream.add');
        }
        else{
            \DB::table('streams')
            ->where('id','=',$id)
            ->update(
                ['stream_name'=>$request->get('stream_name'),
                    'description'=>$request->get('description')]);
            flash('Stream Updated')->success();                        
            return \Redirect::route('stream.add');
        }
    }
    public function destroy($id)
    {
        $stream=Stream::find($id);
        $stream->delete();
        flash('Stream Removed')->success();            
        return \Redirect::route('stream.add');
    }
}
