<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\BranchRequest;
use App\Branch;
use App\User;

class BranchController extends Controller
{
    public function branch()
    {
        $edit="null";
        $branch=Branch::all();
        $user=User::pluck('id');
        return view('admin.pages.branches',compact('edit','branch','user'));
    }

    public function branch_store(BranchRequest $request)
    {        
        $branch = Branch::store($request);
        flash('Branch Added')->success();
        return back();     
    }

    public function branch_edit($id)
    {        
        $edit=Branch::where('id','=',$id)->first();        
        $branch=Branch::all();
        return view('admin.pages.branches',compact('edit','branch'));
    }

    public function branch_editstore(Request $request,$id)
    {       
        \DB::table('branches')
        ->where('id','=',$id)
        ->update(
            [   'branch'=>$request->get('branch'),
                'branch_info'=>$request->get('branch_info'),
                'address'=>$request->get('address'),
                'contact_number'=>$request->get('contact_number'),
                'email'=>$request->get('email')
            ]
        );
        flash('branch Updated')->success();                        
        return \Redirect::route('branch');
    }

    public function destroy($id)
    {
        $branch=Branch::find($id);
        $branch->delete();
        flash('Branch Removed')->success();            
        return \Redirect::route('branch');
    }
}
