<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\JobtypeRequest;
use App\Branch;
use App\Jobtype;

class JobtypeController extends Controller
{
    public function jobtype()
    {
        $branch=Branch::all();
        $jobtype=Jobtype::all();
        $edit="null";
        return view('admin.pages.jobtype',compact('branch','jobtype','edit'));
    }    

    public function jobtype_store(JobtypeRequest $request)
    {
        $query=Jobtype::where('branch_id','=',$request->get('branch_id'))
            ->where('jobtitle','=',$request->get('jobtitle'))->first();

        if(empty($query)){
            $jobtype = Jobtype::store($request);
            flash('Jobtype Data Created')->success();
            return back();
        }        
        else {
            flash('Do Not Add Same Job In Same City')->error();
            return back();
        }
    }
    
    public function edit($id)
    {        
        $edit=Jobtype::where('id','=',$id)->first();        
        $jobtype=Jobtype::all();
        $branch=Branch::all();
        return view('admin.pages.jobtype',compact('edit','jobtype','branch'));
    }

    public function editstore(JobtypeRequest $request,$id)
    {       
        \DB::table('jobtypes')
        ->where('id','=',$id)
        ->update(
            ['branch_id'=>$request->get('branch_id'),
                'jobtitle'=>$request->get('jobtitle'),
                'job_introduction'=>$request->get('job_introduction'),
                'job_description'=>$request->get('job_description')
            ]
        );
        flash('Job Updated Succesfully')->success();                        
        return \Redirect::route('jobtype');
    }

    public function destroy($id)
    {
        $jobtype=Jobtype::find($id);
        $jobtype->delete();
        flash('Job Remove Succesfully')->success();            
        return \Redirect::route('jobtype');
    }
}
