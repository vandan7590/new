<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmailJob;

class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function dashboard()
    {        
        return view('admin.pages.dashboard');
    }
}


//Array Brackets Remove
// $skip = ["[","]","\"","{","}"];
// $interviewschedule = str_replace($skip,' ',$interviewschedule);
