<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoundRequest;
use App\Http\Requests\RoundupdateRequest;
use App\Round;
use App\Examallocate;
use App\Level;
use App\Jobtype;
use App\User;
use App\Jobapplication;

class RoundController extends Controller
{
    public function list_view()
    {
        $jobapplication=Jobapplication::all();
        return view('admin.pages.employeeview',compact('jobapplication'));
    }

    public function view($id)
    {
        $user=User::where('role','=','user')->where('id','=',$id)->get();
        $interview=Round::all();
        $level = Level::all();
        $exam = Examallocate::all();
        $emp_view=Jobtype::pluck('id');
        $jobapplication = Jobapplication::where('user_id','=',$id)->first();
        $status=Round::where('user_id','=',$id)->get();
        return view('admin.pages.interviewschedule',compact('emp_view','interview','user','jobapplication','level','exam'));
    }

    public function round_store(RoundRequest $request,$id)
    {
        $mail = jobapplication::where('user_id','=',$id)->first();
         
        $array = [
            'name'=>'byteandbits',
            'mail'=>$mail->users->name,
            'round_type'=>$request->round_type,
            'date_time'=>$request->date_time,
        ];
        $query=Round::where('user_id','=',$request->get('user_id'))
            ->where('round_type','=',$request->get('round_type'))->first();
            
            if(empty($query)){
            $round = Round::store($request);

            \Mail::send('mail.mail',$array, function($message) use($array,$mail){
                $message->to($mail->users->email)->subject
                ('Laravel Round Registration');
                $message->from('userdemo794@gmail.com');
            });
            flash('Round Added')->success();
            return back();
        }
        flash('Choose Valid Round')->error();
        return back();
    }
    
    public function reschedule_update(RoundupdateRequest $request,$id)
    {
        $mail = jobapplication::where('user_id','=',$id)->first(); 
        $array = [
            'name'=>'byteandbits',
            'reschedule_date'=>$request->reschedule_date];
        
            \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'request_note'=>$request->get('request_note'),
                'verify'=>"1"
                ]);

        \Mail::send('mail.reschedulemail',$array, function($message) use($array,$mail)
        {
            $message->to($mail->users->email)->subject('Reschedule Requested Approval');
            $message->from('userdemo794@gmail.com');
        });
        flash('Reschedule Request Updated')->success();
        return back();
    }

    public function reschedule_reject($id)
    {       
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'verify'=>"2"
            ]);
            $mail = jobapplication::where('user_id','=',$id)->first(); 
            \Mail::send('mail.reject',['name'=>"byteandbits"], function($message) use($mail)
            {
                $message->to($mail->users->email)->subject('Reschedule Requested Cancelled!');
                $message->from('userdemo794@gmail.com');
            });
            flash('Reschedule Request Cancelled')->error();
            return back();
    }

    public function reschedule_confirm($id)
    {
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([            
                'verify'=>"1"
                ]);
                $mail = jobapplication::where('user_id','=',$id)->first(); 
                \Mail::send('mail.empty',['name'=>"byteandbits"], function($message) use($mail)
                {
                    $message->to($mail->users->email)->subject('Reschedule Requested Confirmed !');
                    $message->setbody("Hello Candidate's Your Reschedule Request Accepted.");
                    $message->from('userdemo794@gmail.com');
                });
                flash('Reschedule Request Updated')->success();
                return back();
    }

    public function status_reject($id)
    {
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'status'=>'reject'
            ]);
            return back();
    }

    public function status_update($id, $status)
    {
        \DB::table('rounds')
            ->where('id','=',$id)
            ->update([   
                'status'=>$status]);
            flash('Status Updated')->success();                        
            echo $status;
    }

    public function confirm($id)
    {
        $mail = jobapplication::where('user_id','=',$id)->first();
        $data = array('name'=>'byteandbits','mail' => $mail);
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'confirm'=>'1'
            ]);
            
        \Mail::send('mail.confirmation',$data, function($message) use($mail)
        {
            $message->to($mail->users->email)->subject('Job Confirmation');
            $message->from('userdemo794@gmail.com');
        });
        flash('Selected..')->success();
        return back();
    }

    public function reject($id)
    {
        $mail = jobapplication::where('user_id','=',$id)->first();
        $data = array('name'=>'byteandbits','mail' => $mail);
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'confirm'=>'2'
            ]);
            
            \Mail::send('mail.empty',['name'=>"byteandbits"], function($message) use($mail)
            {
                $message->to($mail->users->email)->subject('Application Rejected');
                $message->setbody("Hello Candidate's I Extremely Sorry, Your Application Not Accepted.");
                $message->from('userdemo794@gmail.com');
            });
            flash('Denied !')->error();
            return back();        
    }

    public function exam_approval($id)
    {
        \DB::table('jobapplications')
            ->where('user_id','=',$id)
            ->update(['status'=>'1']);
            return back();
            flash('Exam Approved')->success();
    }

    public function exam_denied($id)
    {
        \DB::table('jobapplications')
            ->where('user_id','=',$id)
            ->update(['status'=>'2']);
            return back();
            flash('Exam Approved')->error();
    }

    public function level_update($id, $level)
    {
        \DB::table('examallocates')
            ->where('user_id','=',$id)
            ->update([
                'level_id'=>$level]);
            flash('Exam Updated')->success();
            echo $level;
    }
}
