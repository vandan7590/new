<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests\ReferenceRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reference;

class ReferenceController extends Controller
{
    public function reference_add()
    {
        $edit="null";                
        $reference=Reference::all();          
        return view('admin.training.reference',compact('edit','reference'));
    }
    public function reference_store(ReferenceRequest $request)
    {                 
        $description=$request->get('description');       
        if(empty($description))
        {            
            $reference=new Reference(array(
                'reference_name'=>$request->get('reference_name'),            
                'description'=>'--------',
            ));
            $reference->save();
            flash('Reference Added')->success();
            return \Redirect::route('reference.add');
        }           
        else
        {            
            $reference=new Reference(array(
                'reference_name'=>$request->get('reference_name'),            
                'description'=>$request->get('description'),
            ));
            $reference->save();
            flash('Reference Added')->success();
            return \Redirect::route('reference.add');
        }
    }

    public function reference_edit($id)
    {
        $edit=Reference::where('id','=',$id)->first();
        $reference=Reference::all();
        return view('admin.training.reference',compact('edit','reference'));
    }
    public function reference_editstore(Request $request,$id)
    { 
        $description=$request->get('description');       
        if(empty($description))
        {           
            \DB::table('references')
            ->where('id','=',$id)
            ->update(
                ['reference_name'=>strtoupper($request->get('reference_name')),
                 'description'=>'----']);
            flash('Reference Updated')->success();
            return \Redirect::route('reference.add');
        }
        else{
            \DB::table('references')
            ->where('id','=',$id)
            ->update(
                ['reference_name'=>strtoupper($request->get('reference_name')),
                 'description'=>$request->get('description')]);
            flash('Reference Updated')->success();
            return \Redirect::route('reference.add');
        }
    }
    
    public function destroy($id)
    {
        $reference=Reference::find($id);
        $reference->delete();
        flash('Reference Removed')->success();
        return \Redirect::route('reference.add');
    }
}
