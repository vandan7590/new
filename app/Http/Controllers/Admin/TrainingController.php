<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\TrainingRequest;
use App\Http\Requests\DemoroundRequest;
use App\Http\Requests\RescheduleRequest;
use App\Trainingapply;
use App\Document;
use App\Payment;
use App\User;

class TrainingController extends Controller
{
    public function training_view()
    {
        $training = Trainingapply::all();
        return view('admin.training.trainingview',compact('training'));
    }

    public function view($id)
    {
        $payment = Payment::where('user_id','=',$id)->get();
        $training = Trainingapply::where('user_id','=',$id)->first();
        $paid_amount=array_sum(Payment::where('user_id','=',$id)->pluck('amount')->toArray());
        $remaing_amount=($training->fees-$paid_amount);
        $document = Document::where('user_id','=',$id)->get();
        return view('admin.training.trainingschedule',compact('user','training','paid_amount','remaing_amount','payment','document'));
    }

    public function reschedule_reject($id)
    {
        $mail = Trainingapply::where('user_id','=',$id)->first();
            
        \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update([
                'verify'=>'3'
            ]);
            \Mail::send('mail.empty',['name'=>"byteandbits"], function($message) use($mail)
            {
                $message->to($mail->users->email)->subject('Reschedule Requested Cancelled!');
                $message->setbody("Hello Candidate's I Extremely Sorry, Your Reschedule Request Not Accepted.");
                $message->from('userdemo794@gmail.com');
            });
            flash('Reschedule Request Rejected')->error();
            return back();
    }

    public function demo_session(DemoroundRequest $request,$id)
    {
        $mail = Trainingapply::where('user_id','=',$id)->first();
        $data = array('mail' => $mail);
        $array= [
            'name'=>'byteandbits',
            'demo_date'=>$request->demo_date];
            
            \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update(['demo_date'=>$request->get('demo_date')]);
            
            \Mail::send('mail.demoschedule',$array,function($message) use($array,$mail)
            {
                $message->to($mail->users->email)->subject('Demo Schedule');
                $message->from('userdemo794@gmail.com');
            });
            flash('Demo Session Date Added')->success();
            return back();
    }

    public function demo_reschedulerequest(RescheduleRequest $request,$id)
    {
        $mail = Trainingapply::where('user_id','=',$id)->first(); 
        $array = [
            'name'=>'byteandbits',
            'reschedule_date'=>$request->reschedule_date];

        \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'request_note'=>$request->get('request_note'),
                'verify'=>'2'
                ]);
            \Mail::send('mail.reschedulemail',$array, function($message) use($array,$mail)
            {
                $message->to($mail->users->email)->subject('Reschedule Requested Approval');
                $message->from('userdemo794@gmail.com');
            });
            flash('Reschedule Request Added')->success();
            return back();
    }

    public function approval($id)
    {
        \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'yes'
            ]);
            flash('Training Application Approval')->success();
            return back();
    }

    public function cancel($id)
    {
        \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'no'
            ]);
            flash('Training Application Cancelled')->error();
            return back();
    }

    public function doc_approval($id)
    {
        \DB::table('documents')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'yes'
            ]);
            $mail = Trainingapply::where('user_id','=',$id)->first();

            \Mail::send('mail.empty',['name'=>'byteandbits'], function($message) use($mail)
            {
                $message->to($mail->users->email)->subject('Document Verification Done');
                $message->setbody("Hello Candidate's Your Document Verified.");
                $message->from('userdemo794@gmail.com');
            });
            flash('Documents Approval')->success();
            return back();
    }

    public function doc_cancel($id)
    {
        \DB::table('documents')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'no'
            ]);
            flash('Documents Cancelled')->error();
            return back();
    }

    public function charges(Request $request,$id)
    {
        $charges = $request->get('charges');
        $discount = $request->get('discount');
        if($request->get('charges') > 0){
            $finalamount=$charges-$discount;
            \DB::table('trainingapplies')
            ->where('user_id','=',$id)
            ->update(
                ['charges'=>$finalamount]);
            flash('Charges Set')->success();
            return \Redirect::back();
        }
        else {
            flash('Please Enter Valid Amount')->error();
            return \Redirect::back();
        }
    }

    public function paymentstore(Request $request,$id)
    {      
        $fees = Trainingapply::where('id','=',\Auth::user()->id)->pluck('fees')->first();
        $paid_amount = array_sum(Payment::where('user_id','=',$id)->pluck('amount')->toArray());
        $remaing_amount=($fees-$paid_amount);

        if($remaing_amount >= $request->get('amount') && $request->get('amount') > 0){
            $payment=new Payment(array(
                'user_id'=>$request->get('user_id'),
                'training_id'=>$request->get('training_id'),
                'amount'=>$request->get('amount'),
                'due_date'=>$request->get('due_date'),            
            ));
            $payment->save();

            $mail = Trainingapply::where('user_id','=',$id)->first();
            $array = [
                'name' => 'byteandbits',
                'amount' => $request->amount,
                'due_date' => $request->due_date,
                'remaing_amount' => $remaing_amount];
                
                \Mail::send('mail.payment',$array, function($message) use($array,$mail){
                    $message->to($mail->users->email)->subject('Payment Confirmation');
                    $message->from('userdemo794@gmail.com');
                });
                flash('Payment Added')->success();
                return redirect()->back();
        }
        else{
            flash('Please Enter Valid Amount')->error();            
            return redirect()->back();   
        }
    }

    public function print($id)
    {
        $payment = Payment::where('user_id','=',$id)->first();
        $training = Trainingapply::where('user_id','=',$id)->first();
        $paid_amount=array_sum(Payment::where('user_id','=',$id)->pluck('amount')->toArray());
        $remaing_amount=($training->fees-$paid_amount);
        return view('admin.training.print',compact('user','training','paid_amount','remaing_amount','payment'));
    }
}
