<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RescheduleRequest;
use App\Demosession;

class DemosessionController extends Controller
{
    public function demosession_view()
    {
        $demo = Demosession::all();
        return view('admin.demosession.demosessionview',compact('demo'));
    }

    public function demoschedule_view($id)
    {        
        $demo = Demosession::where('user_id','=',$id)->first(); 
        return view('admin.demosession.demoschedule',compact('demo'));
    }

    public function demodate(Request $request,$id)
    {
        \DB::table('demosessions')
        ->where('user_id','=',$id)
        ->update([
            'demodate'=>$request->get('demodate')]);
            flash('Demo Session Date Added')->success();
            return back();
    }

    public function rescheduledate(RescheduleRequest $request,$id)
    {     
        \DB::table('demosessions')
            ->where('user_id','=',$id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'verify'=>'2'
                ]);
                flash('Reschedule Request Added')->success();
                return back();
    }

    public function reschedule_reject($id)
    {
        \DB::table('demosessions')
            ->where('user_id','=',$id)
            ->update([
                'verify'=>'3'
            ]);
            flash('Reschedule Request Rejected')->error();
            return back();
    }

    public function approval($id)
    {
        \DB::table('demosessions')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'yes'
            ]);
            flash('Demosession Visited')->success();
            return back();
    }

    public function cancel($id)
    {
        \DB::table('demosessions')
            ->where('user_id','=',$id)
            ->update([
                'approval'=>'no'
            ]);
            flash('Demosession Cancelled')->error();
            return back();
    }
}
