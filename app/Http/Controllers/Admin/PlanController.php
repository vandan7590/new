<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PlanRequest;
use App\Plan;

class PlanController extends Controller
{
    public function plan_view()
    {
        $edit="null";
        $plan=Plan::all();
        return view('admin.training.plan',compact('edit','plan'));
    }

    public function plan_store(PlanRequest $request)
    {
        $plan = Plan::store($request);
            flash('Plan Added')->success();
            return back();
    }

    public function edit($id)
    {        
        $edit=plan::where('id','=',$id)->first();        
        $plan=Plan::all();
        return view('admin.training.plan',compact('edit','plan'));
    }

    public function editstore(PlanRequest $request,$id)
    {       
        \DB::table('plans')
        ->where('id','=',$id)
        ->update(
            ['plan_name'=>$request->get('plan_name'),
                'description'=>$request->get('description'),
                'price'=>$request->get('price')]);

        flash('Plan Updated')->success();                        
        return \Redirect::route('plan');
    }

    public function plan_destroy($id)
    {
        $plan=Plan::find($id);
        $plan->delete();
        flash('Job Removed')->success();
        return \Redirect::route('plan');
    }
}
