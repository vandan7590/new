<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use IlluminateFoundationAuthThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
        *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $userSocial = Socialite::driver('facebook')->user();
        $findUser =User::where('email',$userSocial->email)->first();
        if($findUser){
            Auth::login($findUser);
            return \Redirect::route('dashboard');
        }else {
            $user =new User;
            $user->role="user";
            $user->name = $userSocial->name;
            $user->email = $userSocial->email;
            $user->save();
            Auth::login($user);
            return \Redirect::route('dashboard');
        }
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function redirect()
    {
        return Socialite::driver('linkedin')->redirect();
    }

    public function Callback()
    {
        try {
            $user = Socialite::driver('linkedin')->user();
            $create['name'] = $user->name;
            $create['email'] = $user->email;
            $create['linkedin_id'] = $user->id;
            
            $userModel = new User;
            $createdUser = $userModel->addNew($create);
            Auth::loginUsingId($createdUser->id);
            return redirect()->route('home');
        } catch (Exception $e) {
            return redirect('login/linkedin');
        }
    }
}
