<?php

namespace App\Http\Controllers\Login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\SignupRequest;
use App\Http\Requests\OtpRequest;
use App\User;
use App\Branch;
use Auth;

class SessionController extends Controller
{
    public function login()
    {
        $branch=Branch::all();
        return view('login.login',compact('branch'));
    }

    public function loginstore(Request $request)
    {
        if(User::loginUser($request->email,$request->password)){
            if(Auth::User()->role=='admin'){
                flash('Logged In')->success();
                return \Redirect::route('dashboard');
            }
            if(Auth::user()->role=='user' && Auth::user()->app_type=="job"){
                return \Redirect::route('jobapply.view');
            }
            if(Auth::user()->role=='user' && Auth::user()->app_type=="training"){
                return \Redirect::route('training.view');
            }
            if(Auth::user()->role=='user' && Auth::user()->app_type=="saminar"){
                return \Redirect::route('demosession.view');
            }
        }
        else{
            flash('Password Or Email Incorrect')->error();
            return redirect()->back();
        }
    }

    public function signup_store(SignupRequest $request)
    {
        $rndno= rand(1000,9999);

        $user= new User(array(
            'role'=>'user',
            'branch_id'=>$request->get('branch_id'),
            'name'=>$request->get('name'),
            'contact_number'=>$request->get('contact_number'),
            'email'=>$request->get('email'),
            'password'=>bcrypt($request->get('password')),
            'app_type'=>$request->get('app_type'),
            'otp'=>$rndno,
            'verify'=>'0'
        ));
        $user->save();
        \Mail::send('mail.reject',["name" => "Byteandbits"], function($message) use($user,$rndno){
            $message->to($user->email)->subject("Your OTP Is Here");
            $message->setbody($rndno);
            $message->from('userdemo794@gmail.com');
        });
        User::loginUser($request->get('email'),$request->get('password'));
        return \Redirect::route('jobapply.view');
    }

    public function enter_otp()
    {   
        if(\Auth::user()){
            if(!\Auth::user('verify')==0){
                return view('login.otp');
            } 
            else {
                flash('Logged In')->success();
                return \Redirect::route('jobapply.view');
            }  
        }       
        else {
            return \Redirect::route('login');
        }        
    }

    public function otp_store(OtpRequest $request)
    {
        $get_otp=$request->get('get_otp');
        $user=\Auth::user();
        if($user->otp==$get_otp){
            if($user->app_type=="job"){
                \DB::table('users')
                ->where('id','=',$user->id)
                ->update([
                    'verify'=>'1'
                ]);
                return \Redirect::route('jobapply.view');
            }            
            elseif($user->app_type=="training"){
                    \DB::table('users')
                    ->where('id','=',$user->id)
                    ->update([
                        'verify'=>'1'
                    ]);
                    return \Redirect::route('training.view');
            }
            elseif($user->app_type=="saminar"){
                return \Redirect::route('demosession.view');
            }
            else {
                return "Invalid";
            }
        }
        else {
            flash('Invalid OTP')->error();
            return back();
        }    
    }

    public function otp_resend()
    {
        $rndno= rand(1000,9999);
        \DB::table('users')
            ->where('id','=',\Auth::user()->id)
            ->update([
                'otp'=>$rndno
            ]);
            return back();
    }

    public function logout(Request $request)
    {    
        \Auth::logout();
        return \Redirect::route('login');
    }
}
