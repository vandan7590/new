<?php

namespace App\Http\Controllers\Exam;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Jobtype;
use App\Question;
use App\Option;
use App\Level;

class QuestionController extends Controller
{
    public function question()
    {
        $edit = "null";
        $level = Level::all();
        $question = Question::all();
        return view('admin.exam.question',compact('level','edit','question'));
    }

    public function question_add(Request $request)
    {
        $question=new Question(array(
            'level_id' => $request->get('level_id'),
            'question' => $request->get('question'),
            'marks' => $request->get('marks')
        ));
        $question->save();
        
        $id = $question->id;
        $option = $request->get('option');
        foreach($option as $data)
        {
            $option = new Option(array(
                'question_id'=>$id,
                'option'=>$data));
            $option->save();
        }
        flash('Question Added')->success();
        return \Redirect::back();
    }

    public function question_edit($id)
    {
        $edit=Question::where('id',$id)->first();
        $level = Level::all();
        $question = Question::all();
        return view('admin.exam.question', compact('edit','level','question'));
    }

    public function question_editstore(Request $request,$id)
    {
        \DB::table('questions')
        ->where('id','=',$id)
        ->update(['level_id' =>$request->get('level_id'),
                'question' => $request->get('question'),                
                'marks' => $request->get('marks'),
        ]);
        $question_edit=Question::where('id','=',$id)->get();
        $option=$request->get('option');
        foreach($option as $options)
        {
            $destory = Option::where('question_id','=',$id)->first();
            $destory->delete();

            $option = new Option(array(
                'question_id'=>$id,
                'option'=>$options));
            $option->save();
        }
        flash('Question Updated')->success();
        return \Redirect::route('question.add');
    }

    public function question_del($id)
    {
        $question=Question::where('id', $id)->delete();
        flash('Question Removed')->error();
        return back();
    }
}
