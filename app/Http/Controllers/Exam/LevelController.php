<?php

namespace App\Http\Controllers\Exam;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LevelRequest;
use App\Jobtype;
use App\Level;

class LevelController extends Controller
{
    public function level_view()
    {
        $jobtype = Jobtype::all();
        $level = Level::all();
        $edit="null";
        return view('admin.exam.level',compact('jobtype','edit','level'));
    }

    public function level_store(LevelRequest $request)
    {
        $level = Level::store($request);
        flash('Level Added')->success();
        return back();     
    }

    public function level_edit($id)
    {        
        $edit=Level::where('id','=',$id)->first();
        $jobtype = Jobtype::all();
        $level = Level::all();
        return view('admin.exam.level',compact('edit','jobtype','level'));
    }

    public function level_editstore(LevelRequest $request,$id)
    {       
        \DB::table('levels')
        ->where('id','=',$id)
        ->update(['level'=>$request->get('level'),
                    'experience'=>$request->get('experience'),
                    'no_of_questions'=>$request->get('no_of_questions'),
                    'level_time'=>$request->get('level_time')]);
        flash('Level Updated')->success();                        
        return \Redirect::route('level.add');
    }

    public function destroy($id)
    {
        $level=Level::find($id);
        $level->delete();
        flash('Level Removed')->success();            
        return \Redirect::route('level.add');
    }
}
