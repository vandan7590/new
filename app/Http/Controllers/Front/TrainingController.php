<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\RescheduleRequest;
use App\Http\Requests\TrainingRequest;
use App\Trainingapply;
use App\Reference;
use App\Payment;
use App\Stream;
use App\Document;

class TrainingController extends Controller
{
    public function view()
    {
        $user = \Auth::user();
        $stream = Stream::all();
        $reference = Reference::all();
        $document = Document::where('user_id','=',$user->id)->first();
        $trainingapply = Trainingapply::where('user_id','=',$user->id)->first();
        $paid_amount=array_sum(Payment::where('user_id','=',\Auth::user()->id)->pluck('amount')->toArray());
        if(empty($trainingapply)){
            return view('Front.training.trainingapply',compact('user','stream','reference'));
        }        
        elseif($trainingapply->plan == "free" || $trainingapply->plan == "basic" || $trainingapply->plan == "professional" || $trainingapply->plan == "advanced"){
            if($trainingapply->approval=="yes"){
                if(empty($document)){
                    return view('front.training.document',compact('user','stream','reference','trainingapply'));
                }
                else {
                    $payment = Payment::where('user_id','=',\Auth::user()->id)->first();
                    $remaing_amount=($trainingapply->fees-$paid_amount);
                    $sum = ($trainingapply->fees * 100);
                    return view('front.training.payment',compact('user','stream','reference','trainingapply','payment','remaing_amount','paid_amount','sum'));
                }
            }
            else{
                return view('front.training.summary',compact('user','stream','reference','trainingapply'));            
            }     
        }        
    }

    public function store(TrainingRequest $request)
    {
        // $plan = $request->get('plan');
        $trainingapply = Trainingapply::store($request,$request->file('resume'),$request->get('plan'));
        flash('Training Form Submited')->success();
        return back();
    }

    public function payment_option(Request $request)
    {
        \DB::table('payments')
            ->where('user_id','=',\Auth::user()->id)
            ->update([
                'payment_option'=>$request->get('payment_option'),
            ]); 
            flash('Payment Option Added')->success();
            return back();
    }

    public function payment_query(Request $request)
    {
        $payment_query = new Payment(array(
            'user_id'=>$request->get('user_id'),
            'training_id'=>$request->get('training_id'),
            'payment_query'=>$request->get('payment_query')
        ));
        $payment_query->save();
        flash('Payment Related Query Request Submited..')->success();
        return back();
    }

    public function reset_demo()
    {
        \DB::table('trainingapplies')
            ->where('user_id','=',\Auth::user()->id)
            ->update([                
                'demo'=>'yes',
            ]); 
            flash('Your Request Demo Session Submited')->success();
        return back();
    }

    public function demo_reschedulerequest(RescheduleRequest $request)
    {     
        \DB::table('trainingapplies')
            ->where('user_id','=',\Auth::user()->id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'request_note'=>$request->get('request_note'),
                'verify'=>'1'
                ]);
                flash('Your Reschedule Request Submited')->success();
                return back();
    }

    public function document_store(Request $request)
    {
        $data=Input::all();
        foreach($data['document'] as $files => $name)
        {            
            if(!empty($name)){                
                $picName=$files . '.' . $name->getClientOriginalExtension();
                $name->move(base_path() . '/public/training/document/'.$data['user_id'].'/',$picName); 
                
                Document::create([
                    'user_id'=>$request->get('user_id'),
                    'doc_type'=>$files,
                    'doc_path'=>$picName
                ]);                
            }                  
        }
        flash('Document Submited')->success();            
        return \Redirect::back();                                                    
    }

    public function cash_request()
    {
        \DB::table('trainingapplies')
            ->where('user_id','=',\Auth::user()->id)
            ->update(['cash_request'=>'1']);
                flash('Your Cash Request Submited')->success();
                return back();
    }

    public function onlinepayment()
    {
        $trainingapply = Trainingapply::where('user_id','=',\Auth::user()->id)->first();
        \DB::table('trainingapplies')
            ->where('user_id','=',\Auth::user()->id)
            ->update(['cash_request'=>'2']);

        \DB::table('payments')
            ->where('user_id','=',\Auth::user()->id)
            ->update(['amount'=>$trainingapply->fees]);
            return back();
    }

    public function pay()
    {
        return view('front.training.pay');
    }
}