<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RescheduleRequest;
use App\Demosession;
use App\Stream;
use App\Reference;

class DemosessionController extends Controller
{
    public function demosession_view()
    {
        $user = \Auth::user();
        $stream = Stream::all();
        $reference = Reference::all();
        $demo = Demosession::where('user_id','=',$user->id)->first();
        if(empty($demo)){
            return view('front.demosession.demoapplication',compact('stream','reference','user'));
        }
        else{
            if(empty($demo->approval)){
                return view('front.demosession.demostatus',compact('stream','reference','user','demo'));
            }
            else{
                return view('front.demosession.confirmation',compact('user'));
            }            
        }        
    }

    public function demosession_store(Request $request)
    {
        $demo = Demosession::store($request);
        flash('Your Demosession Application Successfully Applied')->success();
        return back(); 
    }

    public function demo_reschedule(RescheduleRequest $request)
    {     
        \DB::table('demosessions')
            ->where('user_id','=',\Auth::user()->id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'request_note'=>$request->get('request_note'),
                'verify'=>'1'
                ]);
                flash('Your Reschedule Request Submited')->success();
                return back();
    }
}
