<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\JobapplicationRequest;
use App\Http\Requests\RoundupdateRequest;
use App\Jobapplication;
use App\Jobtype;
use App\Question;
use App\Branch;
use App\Round;
use App\User;

class FrontController extends Controller
{
    public function front_view()
    {
        $user = \Auth::user();
        $jobtype = Jobtype::where('branch_id','=',$user->branch_id)->get();
        $job = Jobapplication::where('user_id','=',$user->id)->first();
        $status = Round::where('user_id','=',$user->id)->get();            

        if(empty($job->jobtype_id)){
            return view('front.pages.jobapply',compact('user','jobtype','status'));
        }
        else{            
            foreach($status as $statuses){
                if($statuses->confirm == "1"){        
                    return view('front.pages.confirmation',compact('user','status','job'));
                }                
            }
            if($job->status=="1"){  
                $question = Question::all();
                return view('front.pages.exam',compact('user','status','job','jobtype','question'));
            }
            return view('front.pages.status',compact('job','user','status'));
        }
    }

    public function jobapplication_store(JobapplicationRequest $request)
    {        
        $jobapplication = Jobapplication::store($request,$request->file('resume'));        
        flash('Job Application Send Successfully')->success();
        return back();
    }

    public function reschedule_store(RoundupdateRequest $request,$id)
    {   
        \DB::table('rounds')
            ->where('user_id','=',$id)
            ->update([
                'reschedule_date'=>$request->get('reschedule_date'),
                'request_note'=>$request->get('request_note'),
            ]); 
        flash('Your Reschedule Request Submited')->success();
        return back();
    }

    public function confirmationview()
    {
        return view('front.pages.confirmation');
    }

    public function demo()
    {
        $user = \Auth::user();
        return view('front.training.demo',compact('user'));
    }

}
