<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = [
        'branch','branch_info','address','contact_number','email'
    ];

    public function Jobtype()
    {
        return $this->hasMany('App\Jobtype');
    }

    public function user()
    {
        return $this->hasMany('App\User');
    }

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public static function store($branch)
    {
        $branch = Branch::create([
            'user_id'=>$branch->user_id,
            'branch'=>$branch->branch,
            'branch_info'=>$branch->branch_info,
            'address'=>$branch->address,
            'contact_number'=>$branch->contact_number,
            'email'=>$branch->email
        ]);
    }
}
