<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobtype extends Model
{
    protected $fillable = [
        'branch_id','jobtitle','job_introduction','job_description'
    ];

    public function branches()
    {
        return $this->belongsTo('App\Branch','branch_id');
    }

    public function jobapplications()
    {
        return $this->hasMany('App\Jobapplication');
    }

    public static function store($jobtype)
    {
        $jobtype = Jobtype::create([
            'branch_id'=>$jobtype->branch_id,
            'jobtitle'=>$jobtype->jobtitle,
            'job_introduction'=>$jobtype->job_introduction,
            'job_description'=>$jobtype->job_description
        ]);
    }
}
