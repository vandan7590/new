var RegisterationIndex = function () {

    var handleTree = function () {

        $('#registerationTree').jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-tag icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "plugins": ["types"]
        });

        //// handle link clicks in tree nodes(support target="_blank" as well)
        //$('#tree_1').on('select_node.jstree', function(e,data) {
        //    var link = $('#' + data.selected).find('a');
        //    if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
        //        if (link.attr("target") == "_blank") {
        //            link.attr("href").target = "_blank";
        //        }
        //        document.location.href = link.attr("href");
        //        return false;
        //    }
        //});
    };

    var handleValidation = function () {
        var catForm = $('#registerationForm');

        catForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                select_multi: {
                    maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                    minlength: jQuery.validator.format("At least {0} items must be selected")
                }
            },
            rules: {
                name: {
                    minlength: 2,
                    required: true
                }
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            }


        });
    };

    var handleModals = function(){
        // general settings
        $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
            '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
            '</div>' +
            '</div>';

        $.fn.modalmanager.defaults.resize = true;

        //ajax demo:
        var $modal = $('#ajax-modal');

        $(document).on('click','.ajax-demo',function(e){

            e.preventDefault();
            var url = $(this).attr('href');
            // create the backdrop and wait for next modal to be triggered
            $('body').modalmanager('loading');

            setTimeout(function(){
                $modal.load(url, '', function(){
                    $modal.modal();
                });
            }, 1000);

        });

        //$('.ajax-demo').on('click', function(e){
        //
        //
        //});

        $modal.on('click', '.update', function(){
            $modal.modal('loading');
            setTimeout(function(){
                $modal
                    .modal('loading')
                    .find('.modal-body')
                    .prepend('<div class="alert alert-info fade in">' +
                    'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                    '</div>');
            }, 1000);
        });

    };

    var handleDelete = function(){

        //$('#bs_confirmation_demo_1').on('confirmed.bs.confirmation', function () {
        //    alert('You confirmed action #1');
        //});

        $(document).on('confirmed.bs.confirmation','a[data-delete]',function(e){
            e.preventDefault();
            var url = $(this).data('url'),token = $(this).data('token');
            var $modal = $('#ajax-modal');
            $modal.modal('hide');
            $.ajax({
                type: 'DELETE',
                data: {
                  "_token" : token
                },
                url: url,
                success: function (data) {
                    toastr['success']('Registeration Deleted', "Success");
                    window.setTimeout('location.reload()', 500);
                },
                error: function (data) {
                    toastr['error']('There was an error', "Error");
                }
            });
        });

        $(document).on('click','a[data-delete]',function(e){
            e.preventDefault();
            $(this).confirmation('show');
        });
    };

    return {
        //main function to initiate the module
        init: function () {

            handleTree();
            handleValidation();
            handleModals();
            handleDelete();
        }

    };

}();