<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '316197012613114',
        'client_secret' => '7cb3d8099ef742890aefe96a6240187a',
        'redirect' => 'http://localhost/new/public/login/facebook/callback',
    ],

    'linkedin' => [
        'client_id' => '81mckt6rrobfa5',
        'client_secret' => '7UJ9rHOZdzsAk7V3',
        'redirect' => 'http://localhost/new/public/login/linkedin/callback',
        ],
];
