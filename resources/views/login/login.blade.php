@include('admin.layouts.partials.head')
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-login m-login--signin m-login--5" id="m_login" style="background-image: url(admin/assets/app/media/img//bg/bg-3.jpg);">
            <div class="m-login__wrapper-1 m-portlet-full-height">
                <div class="m-login__wrapper-1-1">
                    <div class="m-login__contanier">
                        <div class="m-login__content">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="{{asset('admin/assets/app/media/img/users/login.png')}}">
                                </a>
                            </div>
                            <div class="m-login__title">
                                <h3>WE WORK ON SOME OF THE BEST THINGS.</h3>
                            </div>
                            <div class="m-login__desc">
                                We Belive in creativity, working on new technologies that make a difference in maintaining and developing code more easier and efficient way.
                            </div>
                            <div class="m-login__form-action">
                                <button type="button" id="m_login_signup" class="btn btn-outline-focus m-btn--pill">Get An Account</button>
                            </div>
                        </div>
                    </div>
                    <div class="m-login__border">
                        <div></div>
                    </div>
                </div>
            </div>
            <div class="m-login__wrapper-2 m-portlet-full-height">
                <div class="m-login__contanier">
                    <div class="m-login__signin">
                        @include('flash::message')
                        <div class="m-login__head">
                            <h2 class="m-login__title">Login To Your Account</h2>
                        </div>
                        <form class="m-login__form m-form" method="post" action="{{route('login')}}">
                            {{csrf_field()}}
                            <div class="form-group m-form__group">
                                <h5>Email</h5>
                                <input class="form-control m-input" type="email" placeholder="Enter your Username" name="email">
                            </div>
                            <div class="form-group m-form__group">
                                <h5>Password</h5>
                                <input class="form-control m-input m-login__form-input--last" type="Password" placeholder="Enter your Password" name="password">
                            </div><br>
                            <div class="form-group m-form__group">
                                <input type="submit" class="btn btn-info" value="Submit"/>
                            </div>
                        </form>
                    </div>
                    <div class="m-login__signup">
                        <div class="m-login__head">
                            <h2 class="m-login__title">Sign Up</h2>
                        </div>
                        <form class="m-login__form m-form" action="{{route('signup.store')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group m-form__group">
                                <h5>Name</h5>
                                <input class="form-control m-input" type="text" placeholder="Enter your Name" name="name">
                                <span class="text-red error" style="color:red;">{{$errors->first('name')}}</span>
                            </div><br>
                            <div class="form-group m-form__group">
                                <h5>Email</h5>
                                <input class="form-control m-input" type="email" placeholder="Enter Your Email" name="email">
                                <span class="text-red error" style="color:red;">{{$errors->first('email')}}</span>
                            </div><br>
                            <div class="form-group m-form__group">
                                <h5>Contact Number</h5>
                                <input class="form-control m-input" type="text" placeholder="Enter your Number" name="contact_number">
                                <span class="text-red error" style="color:red;">{{$errors->first('contact_number')}}</span>
                            </div><br>
                            <div class="form-group m-form__group">
                                <h5>Password</h5>
                                <input class="form-control m-input m-login__form-input--last" type="Password" placeholder="Enter your Password" name="password">
                                <span class="text-red error" style="color:red;">{{$errors->first('password')}}</span>
                            </div><br>
                            <div class="form-group">
                                <h5>Branch</h5>
                                <select class="form-control m-input" name="branch_id">
                                    @foreach($branch as $branches)
                                        <option value="{{$branches->id}}">{{$branches->branch}}</option>
                                    @endforeach
                                </select>                                                                
                                <span class="text-red error" style="color:red;">{{$errors->first('branch_id')}}</span>
                            </div><br>                                                       
                            <div class="row m-login__form-sub">
                                <h5>Apply For</h5>
                                <div class="col m--align-left">
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="radio" name="app_type" value="job"> Job
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="radio" name="app_type" value="training"> Training
                                        <span></span>
                                    </label>
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="radio" name="app_type" value="saminar"> Free Seminar
                                        <span></span>
                                    </label>
                                </div>
                                <span class="text-red error" style="color:red;">{{$errors->first('app_type')}}</span>
                            </div><br>
                            <div class="form-group">
                                <input type="submit" class="btn btn-info" value="Submit"/>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</body>
<script src="{{asset('admin/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('admin/assets/snippets/custom/pages/user/login.js')}}" type="text/javascript"></script>
<script src="https://sdk.accountkit.com/en_US/sdk.js"></script>
