<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Byte And Bits</title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
		google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
		active: function() {
			sessionStorage.fonts = true;
		}
		});
	</script>
	<link href="{{asset('admin/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{asset('admin/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
	<link rel="shortcut icon" href="{{asset('admin/assets/demo/default/media/img/logo/favicon.ico')}}" />
	<link href="{{asset('admin/custom.css')}}" rel="stylesheet" type="text/css" />
</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{asset('front/assets/app/media/img//bg/bg-3.jpg);')}}">
			<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
				<div class="m-content">
					<div class="row">
						<div class="col-md-12">		
							<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('otp.store')}}">
								{{csrf_field()}}
								<h5> Check Your Email</h5>
								<h3> Enter Your OTP Here...</h3>
								<span style="color:red;">{{$errors->first('get_otp')}}</span>
								<div class="m-portlet__body">										
									<input type="text" class="form-control m-input" name="get_otp" placeholder="ENTER OTP" id="myText"><br>
									<span id="some_div" style="font-weight: 500; margin-left:5px;"> </span>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-form__actions">
										<button type="submit" class="btn btn-primary">Submit</button>
										<button type="cancel" class="btn btn-danger">Cancel</button>
										<a href="{{route('resend.otp')}}" class="btn btn-success">Resend OTP</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
<script>
	var timeLeft = 120;
	var elem = document.getElementById('some_div');
	var timerId = setInterval(countdown, 1000);

	function countdown() {
		if (timeLeft == -1) {
			clearTimeout(timerId);
			doSomething();
		} else {
			elem.innerHTML = timeLeft + ' seconds remaining';
			timeLeft--;
		}
	}
	function doSomething() {
		document.getElementById("myText").disabled = true;
	}
</script>