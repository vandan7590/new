<html lang="en">
	<head>
		<meta charset="utf-8" />
		<title>Metronic | Login Page - 2</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>
		<link href="{{asset('admin/assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('admin/assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{asset('admin/assets/demo/default/media/img/logo/favicon.ico')}}" />
		<link href="{{asset('admin/custom.css')}}" rel="stylesheet" type="text/css" />
	</head>
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url({{asset('admin/assets/app/media/img//bg/bg-2.jpg);')}}">
				<div class="m-grid__item m-grid__item--fluid m-login__wrapper">
					<div class="m-login__container">						
						<div class="m-login__signin">
							<div class="m-login__head">
								<h3 class="m-login__title">Candidate's Application Form</h3>
							</div>
							<form class="m-login__form m-form" method="post" action="{{route('signup.store')}}">
								{{csrf_field()}} 
								<div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Name" name="name">
									<span class="text-red error">{{$errors->first('name')}}</span>
                                </div>
                                <div class="form-group m-form__group">
									<input class="form-control m-input" type="text" placeholder="Contact Number" name="contact_number">
									<span class="text-red error">{{$errors->first('contact_number')}}</span>
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="email" placeholder="Email" name="email">
									<span class="text-red error">{{$errors->first('email')}}</span>
								</div>
								<div class="form-group m-form__group">
									<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
									<span class="text-red error">{{$errors->first('password')}}</span>
								</div><br>
                                <div><labeL class="gap">Apply For</labeL></div><br>        
                                <div class="m-form__group form-group">
                                    <label class="m-radio m-radio--solid m-radio--state-success space">
                                        <input type="radio" name="app_type" value="job"> <div class="color">Job</div>
                                        <span></span>                                        
                                    </label>
                                    <label class="m-radio m-radio--solid m-radio--state-success space">
                                        <input type="radio" name="app_type" value="training"> <div class="color">Training</div>
                                        <span></span>                                        
                                    </label>
                                    <label class="m-radio m-radio--solid m-radio--state-success space">
                                        <input type="radio" name="app_type" value="saminar"> <div class="color">Seminar</div>
                                        <span></span>                                        
									</label>                         									
								</div>			
								<span class="text-red error">{{$errors->first('app_type')}}</span>                      
								<div class="m-login__form-action">
									<button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">Submit</button>						
								</div>							
							</form>															
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>