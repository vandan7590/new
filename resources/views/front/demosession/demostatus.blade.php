@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-3 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Candidate's Information</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Candidate's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="card">
            <div class="card-header">Summary</div>
            <div class="card-body">
                <p class="card-text"><b>You Have Applied For Our Free Demosession Thank You.</b></p>
                <label>Demo Session Schedule</label>
                @if(empty($demo->demodate))
                    <br><span>Your Demosession Request Underprocess !</span>
                @elseif(empty($demo->verify))
                    <br><span> Your Free Demo Lecture Date: {{$demo->demodate}} </span>
                    <br><br><span>You Can Reschedule Demo ! <a href="#" class="button button-3d button-mini button-rounded button-red" data-toggle="modal" data-target=".bs-example-modal-lg">Reschedule Request</a> </span>
                @elseif($demo->verify=="1")
                    <br><span> Your Reschedule Request Underprocess ! </span>
                @elseif($demo->verify=="2")
                    <br><br><span><b> Your Request Accpeted ! New Date: {{$demo->reschedule_date}} </b></span>
                @elseif($demo->verify=="3")
                    <br><br><span><b> Your Request Canceled ! Date: {{$demo->reschedule_date}} </b></span>
                @endif
            </div>
        </div>
    </div>
{{-- Interview Modal --}}
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Demosession Reschedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                <form method="post" action="{{route('demoreschedule.post',['user_id'=>$user->id])}}">
                        {{csrf_field()}}                        
                        <div class="m-widget1__item">
                            <div class="form-group m-form__group">                                  
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reschedule Date </label>
                                    <input type="date" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">	
                                </div>
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reason </label>
                                    <textarea class="form-control m-input" name="request_note" row="2"> </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>    
                        <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</td>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
{{-- End Interview Modal --}}
@stop