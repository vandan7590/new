@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-3 bottommargin clearfix">
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Candidate's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Candidate's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix" style="margin-left:15%">
        <div class="postcontent">
            <h3>Candidate Details</h3>
            <form style="max-width: 25rem;" action="{{route('demosession.store')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <div class="form-group">
                    <label for="exampleInputEmail1">Stream</label>
                    <select name="stream_id" id="new_select" class="form-control m-input"> 
                        @foreach($stream as $streams)
                            <option value="{{$streams->id}}"> {{$streams->stream_name}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Reference</label>
                    <select name="reference_id" id="new_select" class="form-control m-input"> 
                        @foreach($reference as $references)
                            <option value="{{$references->id}}">{{$references->reference_name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Do You Attend Previous Demosession ?</label>
                    <select name="attend" class="form-control m-input">
                        <option value="yes"> Yes </option>
                        <option value="no"> No </option>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        </div>
    </div>
</div>
@stop