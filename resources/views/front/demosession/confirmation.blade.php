@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-3 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Candidate's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Candidate's Status</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="card">
            <div class="card-header">Summary</div>
            <div class="card-body">
                <p class="card-text"><b>Hello {{$user->name}} How was your Demo Lecture experience? </b></p>
                <p class="card-text"><b>Please Share Your Experience At <a href="http://byteandbits.com/"> byteandbits.com </a> </b></p>
            </div>
        </div>
    </div>
@endsection