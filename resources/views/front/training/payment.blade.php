@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Trainee's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Document Section</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Trainee's Payment Option</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="fancy-title title-bottom-border">
            <h3>Payment Option</h3>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th>Payment Plan</th>
                <th>Pay with Razorpay</th>
                <th>Cash</th>
            </tr>
            </thead>
            <tbody> 
                <tr>                                                                  
                    <td>{{$trainingapply->fees}}</td>
                    <form id="rzp-footer-form">            
                        <div class="pay">
                            <td><button class="razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with Razorpay</button></td>
                        </div>
                        <td><button class="btn btn-info" data-toggle="modal" data-target=".bs-example-modal-lg"> Cash Request </button></td>
                    </form>
                </tr> 
            </tbody>
        </table>
    </div>
    <span style="font-weight: bold;"></span>
    {{-- Cash Modal --}}
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-body">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Payment Details</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <form method="post" action="{{route('cashrequest')}}">
                        {{csrf_field()}}
                    <div class="modal-body">
                        <div class="m-widget1__item">
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <div class="form-group m-form__group">                                 
                                <div class="col-lg-12">
                                    <h3>Bhavnagar</h3>
                                    <label for="exampleInputEmail1"> PLease Visit Byteandbits Krishna Complex, T-8 (Third Floor), Waghawadi Rd. , Above TBZ , Bhavnagar 364001 </label><br><br>
                                    <h3>Ahmedabad</h3>
                                    <label for="exampleInputEmail1"> PLease Visit Byteandbits 811, Safal Prelude, Corporate Rd. , Prahlad Nagar, Ahmedabad 380015 </label><br><br>
                                    <h3>Request To Cash Payment</h3>
                                    <label for="exampleInputEmail1"> If You Request To Cash Payment Click On Submit Button. </label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>    
                        <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</td>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- End Cash Modal --}}
    <script>
        function demoSuccessHandler(transaction) {
            $.ajax({
                method: 'post',
                url: "{!!route('onlinepayment')!!}",
                data: {
                    "_token": "{{ csrf_token() }}"
                },
                complete: function (r) {
                    console.log('complete');
                    console.log(r);
                }
            })
        }
    </script>
    <script>
        var options = {
            key: "{{ env('RAZORPAY_KEY') }}",
            amount: '{!! $sum !!}',
            description: 'fees',
            handler: demoSuccessHandler
        }
    </script>
    <script>
        window.r = new Razorpay(options);
        document.getElementById('paybtn').onclick = function () {
            r.open()
        }
    </script>
@stop
