@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Trainee's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Document Section</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Trainee's Payment Option</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="card">
            <div class="card-header">Summary</div>
            <div class="card-body">
                <p class="card-text"><b>You Have Applied For Our Paid Training Plan "{{$trainingapply->plan}}" - ({{$trainingapply->fees}}) Thank You.</b></p>
                <label>Demo Session</label>
                @if($trainingapply->demo=="no")
                    <br><span>Not Required</span>
                    <br><span>You Can Reset Now ! <a href="{{route('demoreset',$user->id)}}" class="button button-3d button-mini button-rounded button-blue">Reset Demo</a> </span> 
                @else
                    <br><span>We Will Allocate A Demo Session Soon !</span>
                    @if(empty($trainingapply->verify))
                        <br><span>Date: {{$trainingapply->demo_date}} <a href="#" class="button button-3d button-mini button-rounded button-red" data-toggle="modal" data-target=".bs-example-modal-lg">Reschedule Request</a> </span>
                    @elseif($trainingapply->verify=="1")
                        <br><br><span><b> Your Request Is UnderProcess ! </b></span>
                    @elseif($trainingapply->verify=="2")
                        <br><br><span><b> Your Request Accpeted ! New Date: {{$trainingapply->reschedule_date}} </b></span>
                    @elseif($trainingapply->verify=="3")
                        <br><br><span><b> Your Request Canceled ! Date: {{$trainingapply->reschedule_date}} </b></span>
                    @endif
                @endif
            </div>
        </div>
    </div>
{{-- Interview Modal --}}
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Interview Reschedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                <form method="post" action="{{route('reschedulerequest.post',['user_id'=>$user->id])}}">
                        {{csrf_field()}}                        
                        <div class="m-widget1__item">
                            <div class="form-group m-form__group">                                  
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reschedule Date </label>
                                    <input type="date" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">	
                                </div>
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reason </label>
                                    <textarea class="form-control m-input" name="request_note" row="2"> </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>    
                        <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</td>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
{{-- End Interview Modal --}}
@stop