@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Trainee's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Document Section</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Trainee's Payment Option</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="postcontent" style="margin-left: 20%;">
        <h3>Training Application Form</h3>
        <form style="max-width: 40rem;" action="{{route('training.store')}}" enctype="multipart/form-data" method="post">
            {{csrf_field()}}
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="form-group">
                <label for="exampleInputEmail1">Stream</label>
                <select id="new_select" class="form-control m-input" name="stream_id"> 
                    @foreach($stream as $streams)
                        <option value="{{$streams->id}}">{{$streams->stream_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Reference</label>
                <select id="new_select" class="form-control m-input" name="reference_id"> 
                    @foreach($reference as $references)
                        <option value="{{$references->id}}">{{$references->reference_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlFile1">CV</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="resume"> <br> 
                <span>Please Upload CV (Doc,PDF,Image).</span>
                <span class="text-red" style="color:red;">{{$errors->first('resume')}}</span><br>                
            </div>                
            <div class="form-group">
                <label for="exampleInputEmail1">Do You Want Demo Session ?</label>
                <select id="new_select" class="form-control m-input" name="demo"> 
                    <option value="yes"> Yes </option>
                    <option value="no"> No </option>
                </select>
            </div>       
    </div>
</div>
<div class="title-center">
    <h3> Training Price-List</h3>
</div>
<div class="row pricing bottommargin clearfix">
    <div class="col-lg-3">
        <div class="pricing-box">
            <div class="pricing-title">
                <h3>Free</h3>
                <span>Free Training</span>
            </div>
            <div class="pricing-price">
                <span class="price-unit">&#8377;</span>0
            </div>
            <div class="pricing-action">
                <button class="btn btn-danger btn-block btn-lg" name="plan" value="free"> Purchase </button>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="pricing-box">
            <div class="pricing-title">
                <h3>Basic</h3>
                <span>Basic Training</span>
            </div>
            <div class="pricing-price">
                <span class="price-unit">&#8377;</span>12000
            </div>
            <div class="pricing-action">
                <button class="btn btn-danger btn-block btn-lg" name="plan" value="basic"> Purchase </button>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="pricing-box best-price">
            <div class="pricing-title">
                <h3>Professional</h3>
                <span>Professional Training</span>
            </div>
            <div class="pricing-price">
                <span class="price-unit">&#8377;</span>18000
            </div>
            <div class="pricing-action">
                <a href="#" ></a>
                <button class="btn btn-danger btn-block btn-lg bgcolor border-color" name="plan" value="professional"> Purchase </button>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="pricing-box">
            <div class="pricing-title">
                <h3>Advanced</h3>
                <span>Advanced Training</span>
            </div>
            <div class="pricing-price">
                <span class="price-unit">&#8377;</span>27000
            </div>
            <div class="pricing-action">
                <button class="btn btn-danger btn-block btn-lg" name="plan" value="advanced"> Purchase </button>
            </div>
        </div>
    </div>
</div>
</form>
@stop