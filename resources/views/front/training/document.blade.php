@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Trainee's Information</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Document Section</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Trainee's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="fancy-title title-bottom-border">
            <h3>Document Section</h3>
        </div>
        <form method="post" action="{{route('document.store')}}" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="user_id" value="{{$user->id}}">
            <div class="row">
                <div class="col-lg-6 bottommargin">
                    <label>10 <sup>th</sup></label><br>
                    <input name="document[tenmarksheet]" type="file" class="file" data-show-preview="false" onchange="ValidateSingleInput(this);"><br>
                    <span>Please Upload 10<sup>th</sup> Marksheet.(Doc,PDF,Image)</span>
                </div>
                <div class="col-lg-6 bottommargin">
                    <label>12 <sup>th</sup></label><br>
                    <input name="document[twelvemarksheet]" type="file" class="file" data-show-preview="false" onchange="ValidateSingleInput(this);"><br>
                    <span>Please Upload 12<sup>th</sup> Marksheet.(Doc,PDF,Image)</span>
                </div>
                <div class="col-lg-6 bottommargin">
                    <label>Diploma</label><br>
                    <input name="document[diploma]" type="file" class="file" data-show-preview="false" onchange="ValidateSingleInput(this);"><br>
                    <span class="m-form__help">Please Upload Diploma Certificate.(Doc,PDF,Image)</span>
                </div>
                <div class="col-lg-6 bottommargin">
                    <label>Bachelor</label><br>
                    <input name="document[bachelor]" type="file" class="file" data-show-preview="false" onchange="ValidateSingleInput(this);"><br>
                    <span class="m-form__help">Please Upload Bachelor Marksheet.(Doc,PDF,Image)</span>
                </div>
                <div class="col-lg-6 bottommargin">
                    <label>Master</label><br>
                    <input name="document[master]" type="file" class="file" data-show-preview="false" onchange="ValidateSingleInput(this);"><br>
                    <span class="m-form__help">Please Upload Master Marksheet.(Doc,PDF,Image)</span> 
                </div>
                <div class="col-lg-6 bottommargin">
                    <label>Photograph</label><br>
                    <input name="image[]" type="file" class="file" id="image" data-show-preview="false" onchange="validateImage('image')"><br>
                    <span class="m-form__help">Please Upload Image In PNG,JPG,JPEG.</span>
                </div>
            </div>        
            <button class="button button-3d button-mini button-rounded button-red"> Submit </button>
        </form>
    </div>

{{-- IMAGE VALIDATION --}}
<script type="text/javascript">
    function validateImage(id) {
        var formData = new FormData();
        var file = document.getElementById(id).files[0];
        formData.append("Filedata", file);
        var t = file.type.split('/').pop().toLowerCase();
        if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
            alert('Please select a valid image file');
            document.getElementById(id).value = '';
            return false;
        }
        return true;
    }  
</script>
{{-- ALL FILES VALIDATION SCRIPT --}}
<script type="text/javascript">
    var _validFileExtensions = [".jpg", ".jpeg", ".png", ".gif", ".png", ".doc", ".docx", ".pdf"];    
    function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                    }
                }  
                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    oInput.value = "";
                    return false;
                }
            }
        }
        return true;
    } 
</script>
@stop