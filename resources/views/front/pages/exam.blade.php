@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Employee's Information</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Employee's Evalution</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Employee's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">4</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="postcontent">
            <h3>Employee's Test</h3>
            <form style="margin-left:15%;" action="" enctype="multipart/form-data" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-lg-12">
                        {{-- <input type="hidden" name="user_id" value="{{$user->id}}">
                        <input type="hidden" name="level_id" value=" {{Session::get('user')['level_id'] }}">  
                        <input type="hidden" name="true_answer" value="{{Session::get('answer')['true_answer'] }}"> --}}
                        <div class="form-group">
                            <label for="exampleInputEmail1">Question</label>                            
                            @foreach ($question as $row)
                                <h3>{{$question->question}}</h3>
                            @endforeach
                        </div> 
                                            
                </div>                                                               
                <button type="submit" class="btn btn-primary mt-3" style="margin-left: 90%;">Submit</button>
            </form>
        </div>
    </div>
</div>
@stop

{{-- 1 interview schedule
-> job application
1 box (condcut evalution round)
	->allocate test ->test(laravel/php/etc...)
	->list test marks result --}}