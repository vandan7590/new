@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-3 bottommargin clearfix">
                <li>
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Employee's Information</h5>
                </li>
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Employee's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        @if(empty($status[0]))
        <div class="style-msg" style="background-color: #EEE;">
            <div class="sb-msg"><i class="icon-question-sign"></i>
                You Have Sucessfully Applied For Post <b>{{$job->jobtypes->jobtitle}}</b> At <b> {{$user->branches->branch}} </b>. 
                Your Request Is Under Process Please Be Patient. Check Back After Sometime.
            </div>
        </div>
        @else
        @foreach($status as $statuses)
        @if($statuses->round_type=="basic")
            <table class="table">
                <thead>
                <tr>
                    <th>Round</th>
                    <th>Date/Time</th>
                    <th>Status</th>
                    <th>Note</th>
                    <th>Reschedule Satus</th>
                    <th>Reschedule </th>
                </tr>
                </thead>
                <tbody>                                                                
                    <tr>                                                                  
                        <td>{{$statuses->round_type}}</td>
                        <td>{{$statuses->date_time}}</td>
                        @if($statuses->status=="underprocess")                                                                        
                        <td class="underprocess">{{$statuses->status}}</td>
                        @elseif($statuses->status=="Reject")
                        <td class="reject">{{$statuses->status}}</td>
                        @else
                        <td class="confirm">{{$statuses->status}}</td>
                        @endif
                        @if(empty($statuses->note))
                            <td> N/A </td>
                        @else
                            <td>{{$statuses->note}}</td> 
                        @endif                                                                    
                        @if($statuses->status=="underprocess")
                            @if(empty($statuses->reschedule_date))
                                <td>N/A</td>
                            @elseif($statuses->verify=="1")
                            <td> Your Request Has Been Confirmed <br>
                                <h5>{{$statuses->reschedule_date}}</h5></td>  
                            @elseif($statuses->verify=="2")
                            <td>Your Reschedule Request Has Been Denied !<br>
                                <h5>{{$statuses->reschedule_date}}</h5>                                                                            
                            </td>  
                            @else
                            <td><h5>Your Request Under Process</h5></td> 
                            @endif                                                                                                         
                        @endif
                        <td><a href="#" class="button button-3d button-mini button-rounded button-red" data-toggle="modal" data-target=".bs-example-modal-lg">Reschedule</a></td>
                    </tr>                                                            
                </tbody>
                @elseif($statuses->round_type=="interview")
                <tbody>
                <tr>                                                                  
                    <td>{{$statuses->round_type}}</td>
                    <td>{{$statuses->date_time}}</td>
                    @if($statuses->status=="underprocess")                                                                        
                    <td class="underprocess">{{$statuses->status}}</td>
                    @elseif($statuses->status=="Reject")
                    <td class="reject">{{$statuses->status}}</td>
                    @else
                    <td class="confirm">{{$statuses->status}}</td>
                    @endif
                    <td>{{$statuses->note}}</td>
                    @if($statuses->status=="underprocess")
                        @if(empty($statuses->reschedule_date))
                        <td><a href="#" class="button button-3d button-mini button-rounded button-red" data-toggle="modal" data-target=".bs-example-modal-lg">Reschedule</a></td>
                        @elseif($statuses->verify=="1")
                        <td>Request Accepted <br>
                            <h5>{{$statuses->reschedule_date}}</h5></td>                                                                        
                        @else
                        <td><h5>Your Request Under Process</h5></td> 
                        @endif                                                                                                         
                    @endif
                </tr>                                                            
                </tbody>
                @elseif($statuses->round_type=="practical")
                    <tbody>                                                                
                        <tr>                                                                  
                            <td>{{$statuses->round_type}}</td>
                            <td>{{$statuses->date_time}}</td>
                            @if($statuses->status=="underprocess")                                                                        
                            <td class="underprocess">{{$statuses->status}}</td>
                            @elseif($statuses->status=="Reject")
                            <td class="reject">{{$statuses->status}}</td>
                            @else
                            <td class="confirm">{{$statuses->status}}</td>
                            @endif
                            <td>{{$statuses->note}}</td>
                            @if($statuses->status=="underprocess")
                                @if(empty($statuses->reschedule_date))
                                    <td><button type="button" class="btn m-btn--pill btn-outline-success" data-toggle="modal" data-target="#m_modal_1"> Reschedule </button></td>
                                @elseif($statuses->verify=="1")
                                <td>Request Accepted <br>
                                    <h5>{{$statuses->reschedule_date}}</h5></td>                                                                        
                                @else
                                <td><h5>Your Request Under Process</h5></td> 
                                @endif                                                                                                         
                            @endif
                        </tr>                                                            
                    </tbody>
                    @elseif($statuses->round_type=="final")
                    <tbody>                                                                
                        <tr>                                                                  
                            <td>{{$statuses->round_type}}</td>
                            <td>{{$statuses->date_time}}</td>
                            @if($statuses->status=="underprocess")                                                                        
                            <td class="underprocess">{{$statuses->status}}</td>
                            @elseif($statuses->status=="Reject")
                            <td class="reject">{{$statuses->status}}</td>
                            @else
                            <td class="confirm">{{$statuses->status}}</td>
                            @endif
                            <td>{{$statuses->note}}</td>
                            @if($statuses->status=="underprocess")
                                @if(empty($statuses->reschedule_date))
                                    <td><button type="button" class="btn m-btn--pill btn-outline-success" data-toggle="modal" data-target="#m_modal_1"> Reschedule </button></td>
                                @elseif($statuses->verify=="1")
                                <td>Request Accepted <br>
                                    <h5>{{$statuses->reschedule_date}}</h5></td>                                                                        
                                @else
                                <td><h5>Your Request Under Process</h5></td> 
                                @endif                                                                                               
                            @endif
                        </tr>                                                            
                    </tbody>
            </table>
        @endif
        @endforeach
        @endif
    </div>
</div>
{{-- Interview Modal --}}
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Interview Reschedule</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                <form method="post" action="{{route('reschedule.store',['id'=>$user->id])}}">
                        {{csrf_field()}}
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="m-widget1__item">
                            <div class="form-group m-form__group">                                  
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reschedule Date </label>
                                    <input type="date" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">	
                                </div>
                                <div class="col-lg-12">
                                    <label for="exampleInputEmail1"> Reason </label>
                                    <textarea class="form-control m-input" name="request_note" row="2"> </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>    
                        <button type="button" class="btn btn-danger" data-dismiss="modal" >Close</td>
                    </div>
            </form>
            </div>
        </div>
    </div>
</div>
{{-- End Interview Modal --}}
@stop