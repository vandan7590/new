@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="container clearfix">
    <div id="processTabs" class="center">
        <ul class="process-steps process-3 bottommargin clearfix">
            <li>
                <a href="#" class="i-circled i-alt divcenter">1</a>
                <h5>Employee's Information</h5>
            </li>
            <li>
                <a href="#" class="i-circled i-alt divcenter">2</a>
                <h5>Employee's Status</h5>
            </li>
            <li class="active">
                <a href="#" class="i-circled i-alt divcenter">3</a>
                <h5>Confirmation</h5>
            </li>
        </ul>    
    </div>
</div>
@include('flash::message')
<div class="container">
    <div class="tab-content clearfix" id="tab-feeds">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Employee Information</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                <code>Name</code>
                </td>
                <td>{{$user->name}}</td>
            </tr>
            <tr>
                <td>
                <code>Contact Number</code>
                </td>
                <td>{{$user->contact_number}}</td>
            </tr>
            <tr>
                <td>
                <code>Email</code>
                </td>
                <td>{{$user->email}}</td>
            </tr>
            <tr>
                <td>
                <code>Post</code>
                </td>
                <td>{{$job->jobtypes->jobtitle}}</td>
            </tr>
            </tbody>
        </table>
        <h5>Your Are Selected For {{$job->jobtypes->jobtitle}} In Byteandbits Branch {{$user->branches->branch}}.Your Joining Date 29/5/2019.</h5>
    </div>
</div>
@stop