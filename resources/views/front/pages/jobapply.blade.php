@extends('ui.layouts.app')
@section('style')
<style>.process-steps li { pointer-events: none; }</style>
@endsection
@section('script')
<script>
    $(function() {
        $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
        $( ".tab-linker" ).click(function() {
            $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
            return false;
        });
    });
</script>
@endsection
@section('content')
<div class="content-wrap">
    <div class="container clearfix">
        <div id="processTabs" class="center">
            <ul class="process-steps process-4 bottommargin clearfix">
                <li class="active">
                    <a href="#" class="i-circled i-alt divcenter">1</a>
                    <h5>Employee's Information</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">2</a>
                    <h5>Employee's Evalution</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">3</a>
                    <h5>Employee's Status</h5>
                </li>
                <li>
                    <a href="#" class="i-circled i-alt divcenter">4</a>
                    <h5>Confirmation</h5>
                </li>
            </ul>    
        </div>
    </div>
    @include('flash::message')
    <div class="container clearfix">
        <div class="postcontent">
            <h3>Employee Details</h3>
            <form style="margin-left:15%;" action="{{route('jobapply.store')}}" enctype="multipart/form-data" method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-6">
                        <input type="hidden" name="user_id" value="{{$user->id}}">                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Job</label>
                            <select name="jobtype_id" id="new_select" class="form-control m-input">
                                @foreach($jobtype as $job)
                                    <option value="{{$job->id}}"> {{$job->jobtitle}} </option>
                                @endforeach
                            </select>
                        </div> 
                        <div class="form-group">
                            <label for="exampleInputEmail1">Experience</label>
                            <select name="experience" class="form-control m-input">
                                <option value="fresher"> Fresher </option>
                                <option value="o-1"> 0-1 </option>
                                <option value="1-2"> 1-2 </option>
                                <option value="2-3"> 2-3 </option>
                                <option value="3-4"> 3-4 </option>
                                <option value="4-5"> 4-5 </option>
                            </select>
                        </div> 
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleFormControlFile1">CV</label> (Please Upload Doc,pdf,image)
                            <input type="file" class="form-control-file" id="exampleFormControlFile1" name="resume">
                            <span class="text-red" style="color:red;">{{$errors->first('resume')}}</span><br>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Join Type</label>
                            <select name="jointype" class="form-control m-input">
                                <option value="immediately"> Immediately </option>
                                <option value="noticeperiod"> Notice Period 
                            </select>
                        </div> 
                    </div>                
                </div>                                                               
                <button type="submit" class="btn btn-primary mt-3" style="margin-left: 90%;">Submit</button>
            </form>
        </div>
    </div>
</div>
@stop