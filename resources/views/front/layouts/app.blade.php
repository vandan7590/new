<!DOCTYPE html>
<html lang="en">
	@include('front.layouts.partials.head')
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			@include('front.layouts.partials.header')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">    
                @include('front.layouts.partials.sidebar')
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
                    @yield('content')
				</div>
            </div>
			@include('front.layouts.partials.footer')
        </div>
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
        </div>                
        @yield('script')
        <script src="{{asset('front/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('front/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('front/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('front/assets/app/js/dashboard.js')}}" type="text/javascript"></script>                
    </body>
    <script src="{{asset('front/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('front/assets/demo/default/custom/crud/wizard/wizard.js')}}" type="text/javascript"></script>    
    <script>
        $('#flash-overlay-modal').modal();
    </script>
</html>