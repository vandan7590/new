<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="SemiColonWeb" />
	<!-- Stylesheets
    ============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Raleway:300,400,500,600,700|Crete+Round:400i" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/bootstrap.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/swiper.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/dark.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/font-icons.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/animate.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/magnific-popup.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/responsive.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/style.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/style-import.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/custom.css')}}" type="text/css" />
	<link rel="stylesheet" href="{{asset('ui/css/components/bs-filestyle.css')}}" type="text/css" />
	<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1" />    
	<!-- Document Title
	============================================= -->
	<title>Byteandbits</title>
	
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> --}}
</head>
<body class="stretched">
	<!-- Document Wrapper
	============================================= -->
	<div id="wrapper" class="clearfix">
		<!-- Header
		============================================= -->
        @include('ui.layouts.partials.header')
        <!-- #header end -->
        @yield('content')
		<!-- Footer
		============================================= -->
        @include('ui.layouts.partials.footer')
        <!-- #footer end -->
	</div><!-- #wrapper end -->
	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>
	<!-- External JavaScripts
	============================================= -->
	<script src="{{asset('ui/js/jquery.js')}}"></script>
	<script src="{{asset('ui/js/plugins.js')}}"></script>
	<!-- Footer Scripts
	============================================= -->
	<script src="{{asset('ui/js/functions.js')}}"></script>
	<script src="{{asset('ui/js/components/bs-filestyle.js')}}"></script>	
	<script>
		$('#flash-overlay-modal').modal();
	</script>	
</body>
</html>