<header id="header" class="transparent-header" data-sticky-class="not-dark">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <div id="logo" style="margin-top:10px;">
                <h4>Byte And Bits</h4>
            </div>
            <nav id="primary-menu">
                <ul>
                    <li><a href="index.html"><div>{{$user->name}}</div></a></li>
                    <li><a href="{{route('logout')}}"><div>Logout</div></a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>