@extends('admin.layouts.app')
@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Jobtype</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Jobtype Table</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
<div class="m-content">
    <div class="row">
        @if($edit=="null")
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('jobtype.store')}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Jobtype</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Branch</label>
                            <select class="form-control m-input" name="branch_id">
                                @foreach($branch as $branches)
                                    <option value="{{$branches->id}}">{{$branches->branch}} : {{$branches->branch_info}}</option>
                                @endforeach
                            </select>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Jobtitle</label>
                            <input type="text" class="form-control m-input" name="jobtitle">			               
                            <span class="text-red error" style="color:red;">{{$errors->first('jobtitle')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Job Introduction</label>
                            <input type="text" class="form-control m-input" name="job_introduction">	
                            <span class="text-red error" style="color:red;">{{$errors->first('job_introduction')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Job Description</label>
                            <input type="text" class="form-control m-input" name="job_description">			            
                            <span class="text-red error" style="color:red;">{{$errors->first('job_description')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>  
        @else
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('editjobtype.post',['id'=>$edit->id])}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Edit Jobtype</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Branch</label>
                            <select class="form-control m-input" name="branch_id">                
                                <option>{{$edit->branches->branch_info}}</option>
                            </select>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Jobtitle</label>
                            <input type="text" class="form-control m-input" name="jobtitle" value="{{$edit->jobtitle}}">			               
                            <span class="text-red error" style="color:red;">{{$errors->first('jobtitle')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Job Introduction</label>
                            <input type="text" class="form-control m-input" name="job_introduction" value="{{$edit->job_introduction}}">	
                            <span class="text-red error" style="color:red;">{{$errors->first('job_introduction')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Job Description</label>
                            <input type="text" class="form-control m-input" name="job_description" value="{{$edit->job_description}}">			            
                            <span class="text-red error" style="color:red;">{{$errors->first('job_description')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>
        @endif
        <div class="col-md-12">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
                                Display Job
							</h3>
						</div>
					</div>				
				</div>
				<div class="m-portlet__body">						
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">														
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
											<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
								</div>
							</div>						
						</div>
					</div>
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
								<th title="Field #1">Branch</th>
								<th title="Field #2">Jobtype</th>
								<th title="Field #3">Job Information</th>
                                <th title="Field #4">Job Description</th>						
                                <th title="Field #5">Edit</th>
                                <th title="Field #6">Delete</th>												
							</tr>
						</thead>
						<tbody>
							@foreach($jobtype as $jobs)							
							<tr>
								<td>{{$jobs->branches->branch}}</td>
                                <td>{{$jobs->jobtitle}}</td>
                                <td>{{$jobs->job_introduction}}</td>
                                <td>{{$jobs->job_description}}</td>
                                <td><a href="{{route('jobtype.edit',['id'=>$jobs->id])}}" class="btn btn-success"><i class="fa fa-edit"> </i> </a> </td>
                                <td><a href="{{route('jobtype.delete',['id'=>$jobs->id])}}" class="btn btn-danger"><i class="fa fa-trash"> </i> </a> </td>
							</tr>							
							@endforeach			  
						</tbody>
					</table>					
				</div>
			</div>		
		</div>      
    </div>
</div>
@stop

