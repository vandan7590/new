@extends('admin.layouts.app')
@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Branch</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Branch Table</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
<div class="m-content">
    <div class="row">
        {{-- Branch Add --}}
        @if($edit=="null")
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('branch.store')}}" id="myForm" name="myform">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Branches</h3>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Country</label>
                            <select class="form-control m-input" name="country" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select State</label>
                            <select class="form-control m-input" name="state" id="stateSel" size="1">
                                <option value="" selected="selected">Please Select Country First</option>
                            </select>
                        </div>																																
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select City</label>
                            <select class="form-control m-input" name="branch" id="districtSel" size="1">
                                <option value="" selected="selected">Please Select State First</option>
                                <span class="text-red error" style="color:red;">{{$errors->first('branch')}}</span>
                            </select>
                        </div>     
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Branch Info</label>
                            <input type="text" class="form-control m-input" name="branch_info">			               
                            <span class="text-red error" style="color:red;">{{$errors->first('branch_info')}}</span> 
                        </div>                       
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" class="form-control m-input" name="address">			               
                            <span class="text-red error" style="color:red;">{{$errors->first('address')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Contact Number</label>
                            <input type="text" class="form-control m-input" name="contact_number">	
                            <span class="text-red error" style="color:red;">{{$errors->first('contact_number')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control m-input" name="email">			            
                            <span class="text-red error" style="color:red;">{{$errors->first('email')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>        
        @else
        {{-- Branch Edit --}}
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('editbranch.post',['id'=>$edit->id])}}" id="myForm" name="myform">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Branches</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Country</label>
                            <select class="form-control m-input" name="country" id="countySel" size="1">
                                <option value="" selected="selected">Select Country</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select State</label>
                            <select class="form-control m-input" name="state" id="stateSel" size="1">
                                <option value="" selected="selected">Please Select Country First</option>
                            </select>
                        </div>																																
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select City</label>
                            <select class="form-control m-input" name="branch" id="districtSel" size="1">
                                <option value="" selected="selected">Please Select State First</option>
                                <span class="text-red error" style="color:red;">{{$errors->first('branch')}}</span>
                            </select>
                        </div>     
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Branch Info</label>
                            <input type="text" class="form-control m-input" name="branch_info" value="{{$edit->branch_info}}">			               
                        </div>                      
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Address</label>
                            <input type="text" class="form-control m-input" name="address" value="{{$edit->address}}">			               
                            <span class="text-red error" style="color:red;">{{$errors->first('address')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Contact Number</label>
                            <input type="text" class="form-control m-input" name="contact_number" value="{{$edit->contact_number}}">	
                            <span class="text-red error" style="color:red;">{{$errors->first('contact_number')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control m-input" name="email" value="{{$edit->email}}">			            
                            <span class="text-red error" style="color:red;">{{$errors->first('email')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>   
        @endif
        <div class="col-md-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Branch List
                            </h3>
                        </div>
                    </div>				
                </div>
                <div class="m-portlet__body">						
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">														
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>						
                        </div>
                    </div>
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                            <tr>
                                <th title="Field #1">Branch</th>
                                <th title="Field #2">Branch Info</th>
                                <th title="Field #3">Address</th>
                                <th title="Field #4">contact</th>						
                                <th title="Field #5">Email</th>
                                <th title="Field #5"> Edit</th>
                                <th title="Field #6">Delete</th>												
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($branch as $branches)							
                            <tr>
                                <td>{{$branches->branch}}</td>
                                <td>{{$branches->branch_info}}</td>
                                <td>{{$branches->address}}</td>
                                <td>{{$branches->contact_number}}</td>
                                <td>{{$branches->email}}</td>
                                <td><a href="{{route('branch.edit',['id'=>$branches->id])}}" class="btn btn-success"><i class="fa fa-edit"> </i> </a> </td>
                                <td><a href="{{route('branch.delete',['id'=>$branches->id])}}" class="btn btn-danger"><i class="fa fa-trash"> </i> </a> </td>
                            </tr>							
                            @endforeach			  
                        </tbody>
                    </table>					
                </div>
            </div>		
        </div> 
    </div>
</div>
@stop

