@extends('admin.layouts.app')
@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Interview Schedule</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Interview Schedule</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
@foreach($user as $users)
<a href="{{route('confirm',$users->id)}}" class="btn btn-info selectedbutton"> Selected </a>
<a href="{{route('rejected',$users->id)}}" class="btn btn-danger rejectbutton"> Reject </a>
@endforeach
<div class="m-content">
    <div class="row">
        <div class="col-md-12">            
            <div class="m-portlet">                
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>                            
                            <h3 class="m-portlet__head-text">
                                Interview Schedule
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section">
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-success">
                                <thead>
                                    <tr>
                                        <th>Test Name</th>
                                        <th>List Of Test</th>                                        
                                        <th>Result</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>                
                                    <td>
                                    <input type="hidden" id="uid" value="{{$jobapplication->user_id}}" name="user_id">
                                    <select class="form-control m-input" name="level_id" id="level">
                                    @foreach ($level as $row)
                                        <option value="{{$row->id}}"> {{$row->level}} </option>
                                    @endforeach
                                    </select>
                                    <div id="autosave"></div>
                                    </td>
                                    <input type="text" id="updated" name="update" class="input-border"/>
                                    <td></td>                                
                                    <td></td>
                                    <td></td>
                                    </tr>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="row">
        <div class="col-md-12">            
            <div class="m-portlet">                
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>                            
                            <h3 class="m-portlet__head-text">
                                Interview Schedule
                            </h3>
                        </div>
                    </div>
                </div>
                @foreach($user as $users)
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="{{route('round.post',['id'=>$users->id])}}">
                    {{csrf_field()}}
                @endforeach
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <h5>Round</h5>
                            </div>                                       
                        </div>
                        @foreach($user as $users)
                        <input type="hidden" name="user_id" value="{{$users->id}}">
                        @endforeach
                        <input type="hidden" name="jobapplication_id" value="{{$jobapplication->id}}">
                        <div class="form-group m-form__group row">                            
                            <div class="col-lg-4">
                                <label class="">Round Type</label>                                    
                                <select class="form-control m-input" id="exampleSelect1" name="round_type">
                                    <option value="basic"> Basic Round </option>                                        
                                    <option value="interview"> Interview Round </option>                                                                            
                                    <option value="practical"> Practical Round </option>                                                                            
                                    <option value="final"> Final </option>                                                                            
                                </select>                                    
                            </div>
                            <div class="col-lg-4">
                                <label class="">Date & Time</label>
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input" name="date_time" readonly="" placeholder="Select date &amp; time" id="m_datetimepicker_2">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="la la-calendar-check-o glyphicon-th"></i></span>                        
                                    </div>
                                </div>
                                <span class="text-red error" style="color:red;">{{$errors->first('date_time')}}</span>
                            </div>
                            <div class="col-lg-4">
                                <label>Notes</label>
                                <div class="m-input-icon m-input-icon--right">
                                    <input type="text" class="form-control m-input" name="notes">
                                </div>
                            </div>                            
                            <div class="btnleftmargin">
                                <input type="submit" class="btn btn-info" value="Set Round">
                            </div>
                        </div>                    
                    </div>
                </form>              
            </div>
        </div>
    </div>
</div>
<div class="m-content">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        Interview Schedule
                    </h3>
                </div>
            </div>
        </div>        
        <div class="m-portlet__body">
            <table class="table m-table m-table--head-bg-success">
                <thead>
                    <tr>                                                                 
                        <th>Round</th>
                        <th>Round Type</th>
                        <th>Date/Time</th>                                    
                        <th>Status</th>
                        <th>Reschedule Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody> 
                @foreach($interview as $view)                                                                                                           
                <tr>                                                                  
                    <td>{{$view->id}}</td>
                    <td>{{$view->round_type}}</td>
                    <td>{{$view->date_time}}</td>                    
                    @if($view->status=="reject")
                        <td><h5>Reject</h5></td>
                        <td></td>
                    @elseif($view->status=="clear")
                        <td><h5>Round Completed</h5></td>
                        <td></td>
                    @else
                    <input type="hidden" id="hid" value="{{$view->id}}" name="hid">
                    <td>
                        <select class="form-control m-input" id="status" name="status">
                            <option value="underprocess" >Under Process</option>
                            <option value="reject">Reject</option>
                            <option value="clear">Clear</option><br>
                        </select>                  
                        <div id="autosave"></div>                                                           
                    </td>
                    {{-- <input type="text" id="update" name="update" class="input-border"/> --}}
                    @endif
                    @if($view->verify=="2")
                        <td> <span class="btn btn-danger">{{$view->reschedule_date}} (Rejected) </span> </td>
                    @elseif($view->verify=="1")
                        <td> <span class="btn btn-info"> {{$view->reschedule_date}} (Request Confirmed) </span> </td>
                    @else
                        <td>{{$view->reschedule_date}}</td>
                    @endif                    
                    @if($view->status=="reject")
                    @elseif($view->status=="clear")
                    @else
                        @if(empty($view->reschedule_date))
                        <td>
                            <button type="button" class="btn btn-warning space" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button>
                        </td>
                        @elseif($view->verify=="2")
                        <td>    
                            <button type="button" class="btn btn-warning space" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> 
                        </td>
                        @elseif($view->verify=="1")
                            <td> <button type="button" class="btn btn-warning space" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> </td>
                        @else
                        <td>
                            @foreach($user as $users)
                            <a href="{{route('reschedule.update',$users->id)}}" class="btn btn-info"> <i class="fa fa-check" aria-hidden="true"></i> </a>
                            <a href="{{route('reschedule.rejected',$users->id)}}" class="btn btn-danger space"> <i class="fa fa-ban" aria-hidden="true"> </i> </a> 
                            <button type="button" class="btn btn-warning space" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> 
                            @endforeach
                        </td>
                        @endif                        
                    @endif    
                </tr>                                
                @endforeach                           
                </tbody>
            </table>
            <h5><span class="text-red error" style="color:red;">{{$errors->first('reschedule_date')}}</span></h5>
        </div>
    </div>
</div>
{{-- Ajax Status Update  --}}
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @foreach($user as $users)
            <form method="post" action="{{route('reschedule.edit',['id'=>$users->id])}}">
                {{csrf_field()}}
            @endforeach
                <div class="modal-body">                    
                    <div class="m-widget1__item">
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Reschedule Date </label>
                                <input type="date" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">
                                <span class="form-text text-muted">The reschedule date must be a date after today.</span><br>
                            </div>                                                       
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Reason </label>
                                <textarea class="form-control m-input" name="request_note" row="2"> </textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</td>
                </div>
            </form>
        </div>
    </div>
</div>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
{{-- <script type="text/javascript">
    $( "#status" ).change(function() { 
        var id =$("#hid").val();
        var status =$("#status").val();
        console.log(status + " " + id);
        $.ajax({
            type: "GET",
            url: "update/"+ id + "status/" + status,
            success: function(data){
            console.log(data);
            $("#update").val(data);
            },
        });
    });
</script> --}}
<script type="text/javascript">
    $( "#level" ).change(function() { 
        var id =$("#uid").val();
        var level =$("#level").val();
        console.log(id + " " + level);
        $.ajax({
            type: "GET",
            url: "http://localhost/new/public/exam/level/"+id + "/" + level,
            success: function(data){
            console.log(data);
            $("#updated").val(data);
            },
        });
    });
</script>
@stop


