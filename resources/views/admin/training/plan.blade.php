@extends('admin.layouts.app')
@section('content')
<div class="m-subheader ">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Plan</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Plan</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
<div class="m-content">
    <div class="row">
        @if($edit=="null")
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('plan.store')}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Plan</h3>
                        </div>                                              
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Name</label>
                            <input type="text" class="form-control m-input" name="plan_name">
                            <span class="text-red error" style="color:red;">{{$errors->first('plan_name')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Details</label>
                            <textarea class="summernote" name="description"></textarea>
                            <span class="text-red error" style="color:red;">{{$errors->first('description')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Price</label>
                            <input type="text" class="form-control m-input" name="price">
                            <span class="text-red error" style="color:red;">{{$errors->first('price')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>
        @else
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('plan.editstore',['id'=>$edit->id])}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Plan</h3>
                        </div>                                              
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Name</label>
                            <input type="text" class="form-control m-input" name="plan_name" value="{{$edit->plan_name}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('plan_name')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Details</label>
                            <textarea class="summernote" name="description">{{$edit->description}}</textarea>
                            <span class="text-red error" style="color:red;">{{$errors->first('description')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Plan Price</label>
                            <input type="text" class="form-control m-input" name="price" value="{{$edit->price}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('price')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>
        @endif
        <div class="col-md-12">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Display Plan
                            </h3>
                        </div>
                    </div>				
                </div>
                <div class="m-portlet__body">						
                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                        <div class="row align-items-center">
                            <div class="col-xl-8 order-2 order-xl-1">
                                <div class="form-group m-form__group row align-items-center">														
                                    <div class="col-md-4">
                                        <div class="m-input-icon m-input-icon--left">
                                            <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>						
                        </div>
                    </div>
                    <table class="m-datatable" id="html_table" width="100%">
                        <thead>
                            <tr>
                                <th title="Field #1">Plan Name</th>
                                <th title="Field #2">Description</th>
                                <th title="Field #3">Price</th>
                                <th title="Field #4">Edit</th>
                                <th title="Field #5">Delete</th>												
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plan as $plans)							
                            <tr>
                                <td>{{$plans->plan_name}}</td>
                                <td>{!! $plans->description !!}</td>
                                <td>{{$plans->price}}</td>
                                <td><a href="{{route('plan.edit',['id'=>$plans->id])}}" class="btn btn-success"><i class="fa fa-edit"> </i> </a> </td>
                                <td><a href="{{route('plan.delete',['id'=>$plans->id])}}" class="btn btn-danger"><i class="fa fa-trash"> </i> </a> </td>
                            </tr>							
                            @endforeach			  
                        </tbody>
                    </table>					
                </div>
            </div>
        </div>
    </div>
</div>
@stop

