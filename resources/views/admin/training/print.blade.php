@extends('admin.layouts.app')
@section('content')
<button onclick="MyFunction()" style="margin-left:92%; margin-top:10px;" class="btn btn-info">Print</button>
<div class="m-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="m-portlet">
                <div class="m-portlet__body m-portlet__body--no-padding">
                    <div class="m-invoice-2">
                        <div class="m-invoice__wrapper">
                            <div class="m-invoice__head" style="background-image: url(../../assets/app/media/img//logos/bg-6.jpg);">
                                <div class="m-invoice__container m-invoice__container--centered">
                                    <div class="m-invoice__logo">
                                        <a href="#">
                                            <h1>RECEIPT</h1>
                                        </a>
                                        <a href="#">
                                            <img src="{{asset('admin/assets/app/media/img/users/byteandbits.png')}}">
                                        </a>
                                    </div>                                    
                                    <div class="m-invoice__items">
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">Date</span>
                                        <span class="m-invoice__text">{{$payment->due_date}}</span>
                                        </div>
                                        <div class="m-invoice__item">
                                            <span class="m-invoice__subtitle">Plan</span>
                                            <span class="m-invoice__text">{{$payment->trainingapplies->fees}}</span>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="m-invoice__body m-invoice__body--centered">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Installment</th>
                                                <th>Paid Amount</th>
                                                <th>Reaming Amount</th>
                                                <th>Total Amount</th>
                                            </tr>
                                        </thead>
                                        <tbody>                                        
                                            <tr>
                                            <td>Thank You For Payment.</td>
                                            <td>{{$paid_amount}}</td>
                                            <td>{{$remaing_amount}}</td>
                                            <td class="m--font-danger">{{$payment->trainingapplies->fees}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function MyFunction()
    {
        window.print();
    }
</script>
@stop