@extends('admin.layouts.app')
@section('content')
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Training Schedule</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Training Schedule</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div><br>
@include('flash::message')
@if(empty($training->approval)|| $training->approval=="no")
<a href="{{route('approval',$training->user_id)}}" class="btn btn-primary selectedbutton"> Approval </a>
<a href="{{route('cancel',$training->user_id)}}" class="btn btn-danger rejectbutton"> Reject </a>
<div class="container">
    <div class="m-portlet">
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Summary Details
                        </h3>
                    </div>
                </div>
            </div>        
            <div class="m-portlet__body">
                <table class="table m-table m-table--head-bg-success">
                    <thead>
                        <tr>                                                                 
                            <th>Demo</th>
                            <th>Demo session Date</th>
                            <th>Reschedule Date</th>                                    
                            <th>Request Date</th>
                            <th>Request Note</th>
                        </tr>
                    </thead>
                    <tbody>                
                        <tr>
                            <td>{{$training->demo}}</td>                        
                            <td><button type="button" class="btn btn-primary space" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> </td>
                                <span class="text-red error" style="color:red;">{{$errors->first('demo_date')}}</span>                        
                            <td>
                                @if($training->verify=="1")
                                {{$training->reschedule_date}} <button type="button" class="btn btn-primary space" data-toggle="modal" data-target="#m_modal_2"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> 
                                <a href="{{route('reschedule.reject',$training->user_id)}}" class="btn btn-danger space"> <i class="far fa-trash-alt" aria-hidden="true"> </i> </a> </td>
                                @else
                                    N/A
                                @endif        
                            <td>
                                @if(empty($training->verify))
                                    N\A
                                @elseif($training->verify=="1")
                                    {{$training->reschedule_date}}
                                @elseif($training->verify=="2")
                                    Request Accepted
                                @elseif($training->verify=="3")
                                    Request Rejected
                                @endif
                            </td>
                            <td>{{$training->request_note}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@include('flash::message')
@elseif($training->approval=="yes")
    @if(empty($document[0]->approval))
    <div class="container">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>                              
                        </span>                            
                        <h3 class="m-portlet__head-text">
                            Document Details
                            <a href="{{route('doc.approval',$training->user_id)}}" class="btn btn-primary selectdoc"> Approval </a>
                            <a href="{{route('doc.cancel',$training->user_id)}}" class="btn btn-danger"> Reject </a>
                        </h3>                    
                    </div>
                </div>
            </div>              
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="{{route('charges.added',['id'=>$training->user_id])}}">
                {{csrf_field()}}
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        @foreach($document as $documents)
                        <div class="col-lg-4">
                            <a href="{{URL::to('training/document/'.$documents->doc_type)}}"> {{$documents->doc_type}} </a>
                        </div>
                        @endforeach                           
                    </div>
                </div>
            </form>              
        </div>
    </div>
    @else
    <div class="container">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>                              
                        </span>                            
                        <h3 class="m-portlet__head-text">
                            Payment Plan Details
                        </h3>
                    </div>
                </div>
            </div>              
            <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="post" action="{{route('payment.store',['user_id'=>$training->user_id])}}">
                {{csrf_field()}}
                <div class="form-group m-form__group m--margin-top-10 m--hide">
                    <input type="text" name="user_id" value="{{$training->user_id}}">
                    <input type="text" name="training_id" value="{{$training->id}}">
                </div>
                <div class="m-portlet__body">
                    <div class="form-group m-form__group row">
                        <div class="col-lg-4">
                            Payment Plan
                        </div>                                       
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-4">
                            <label class="btn btn-primary">Charges: {{$training->fees}}</label> 
                        </div>                            
                        <div class="col-lg-4">
                            <label class="btn btn-danger">Remaing: {{$remaing_amount}}</label>
                        </div>
                        <div class="col-lg-4">
                            <label class="btn btn-success">Paid Amount: {{$paid_amount}}</label>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-lg-4">
                            <input type="text" name="amount" class="form-control m-input" id="amount" placeholder="Amount">
                        </div>
                        <div class="col-lg-4">
                            <input type="date" class="form-control m-input" name="due_date" placeholder="Due Date">
                        </div>
                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-info"> Submit </button>
                        </div>                             
                    </div>
                </div>
            </form>              
        </div>
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                            <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                            Trainee Payment Details
                        </h3>
                    </div>
                </div>
            </div>       
            <div class="m-portlet__body">
                @if($training->cash_request == "2")
                    <h6><b>Request To Online Payment.</b></h6> 
                @elseif($training->cash_request == "1")
                    <h6><b>Request To Cash Payment.</b></h6> 
                @endif
                <br>
                <table class="table m-table m-table--head-bg-success">
                    <thead>
                        <tr>                                                                 
                            <th>Trainee Name</th>
                            <th>Charges</th>
                            <th>Paid Amount</th>                                    
                            <th>Due Date</th>
                            <th>Payment Query</th>
                            <th>Print</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($payment as $pay)
                        <tr>
                            <td>{{$pay->users->name}}</td>
                            <td>{{$pay->trainingapplies->fees}}</td>
                            <td>{{$pay->amount}}</td>
                            <td>{{$pay->due_date}}</td>
                            <td>{{$pay->payment_query}}</td>
                            <td><a href="{{route('print',['user_id'=>$pay->user_id])}}" class="btn btn-info"><i class="fa fa-file-photo-o"> Print </i> </a> </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
@endif
{{-- Demo Session Date --}}
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('demosessiondate.post',['id'=>$training->user_id])}}">
                {{csrf_field()}}
                <div class="modal-body">                    
                    <div class="m-widget1__item">
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Demo Session Date </label>
                                <input type="datetime-local" class="form-control m-input" name="demo_date" aria-describedby="emailHelp" required="">
                            </div>                                                                                   
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</td>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Rescedule Date Accepted --}}
<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('demo_reschedule.post',['id'=>$training->user_id])}}">
                {{csrf_field()}}
                <div class="modal-body">                    
                    <div class="m-widget1__item">
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Reschedule Date </label>
                                <input type="date" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">
                            </div>                                                                                   
                        </div>
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Request Note </label>
                                <input type="text" class="form-control m-input" name="request_note" aria-describedby="emailHelp">
                            </div>                                                                                   
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</td>
                </div>
            </form>
        </div>
    </div>
</div>
@stop