@extends('admin.layouts.app')
@section('content')
@if($errors->any())
@include('errors.errors') 
@endif
@include('flash::message')
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title m-subheader__title--separator">REFERENCE</h3>
			<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
				<li class="m-nav__item m-nav__item--home">
					<a href="#" class="m-nav__link m-nav__link--icon">
						<i class="m-nav__link-icon la la-home"></i>
					</a>
				</li>
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="{{route('dashboard')}}" class="m-nav__link">
						<span class="m-nav__link-text">Home</span>
					</a>
				</li>
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="#" class="m-nav__link">
						<span class="m-nav__link-text">Reference view</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="m-content">
	<div class="row">
		<div class="col-md-4">
			@if($edit=="null")
			<div class="m-portlet m-portlet--tab">											
				<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('reference.add')}}">
					{{csrf_field()}}
					<div class="m-portlet__body">
						<div class="form-group m-form__group m--margin-top-10">
							<h3>Add New Reference</h3>
						</div>
						<div class="form-group m-form__group">
							<label for="exampleInputEmail1">Reference Name <span class="star">*</span></label>
							<input type="text" class="form-control m-input" name="reference_name" aria-describedby="emailHelp">												
							max 100 chars
						</div>																																
						<div class="form-group m-form__group">
							<label for="exampleTextarea">Description (Optional)</label>
							<textarea class="form-control m-input" name="description" rows="3"></textarea>
							max 150 chars
						</div>
					</div>
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="reset" class="btn btn-secondary">Cancel</button>
						</div>
					</div>
				</form>
				<!--end::Form-->
			</div>
			<!--end::Portlet-->                                
			@else
			<div class="m-portlet m-portlet--tab">																		
				<form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('editreference.post',['id'=>$edit->id])}}">
					{{csrf_field()}}										
					<div class="m-portlet__body">
						<div class="form-group m-form__group m--margin-top-10">
							<h3>Edit Reference</h3>
						</div>						
						<div class="form-group m-form__group">
							<label for="exampleInputEmail1">Reference Name</label>
							<input type="text" class="form-control m-input" name="reference_name"  value="{{$edit->reference_name}}" aria-describedby="emailHelp">												
							max 100 chars
						</div>																					
						<div class="form-group m-form__group">
							<label for="exampleTextarea">Description</label>
							<textarea class="form-control m-input" name="description" value="" rows="3">{{$edit->description}}</textarea>
							max 150 chars
						</div>											
					</div>										
					<div class="m-portlet__foot m-portlet__foot--fit">
						<div class="m-form__actions">
							<button type="submit" class="btn btn-primary">Submit</button>
							<button type="reset" class="btn btn-secondary">Cancel</button>
						</div>
					</div>
				</form>																		
			</div>
		@endif
		</div>
		<div class="col-md-8">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
							Reference Data View Table
							</h3>
						</div>
					</div>				
				</div>
				<div class="m-portlet__body">						
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">														
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
											<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
								</div>
							</div>						
						</div>
					</div>
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
								<th title="Field #1" data-field="OrderID">Reference Name</th>
								<th title="Field #2" data-field="Owner">Description</th>
								<th title="Field #3" data-field="Contact">Edit</th>
								<th title="Field #4" data-field="CarMake">Delete</th>						
							</tr>
						</thead>
						<tbody>
							@foreach ($reference as $references)							
							<tr>
								<td>{{$references->reference_name}}</td>
								<td>{{$references->description}}</td>
								<td><a href="{{route('reference.edit',['id'=>$references->id])}}" class="btn btn-success"><i class="fa fa-edit"> </i> </a> </td>
								<td><a href="{{route('reference.delete',['id'=>$references->id])}}" class="btn btn-danger"><i class="fa fa-trash"> </i> </a> </td>
							</tr>							
							@endforeach			
						</tbody>
					</table>					
				</div>
			</div>		
		</div>						                            							
	</div>                		
</div>
@stop
