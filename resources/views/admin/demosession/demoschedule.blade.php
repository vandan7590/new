@extends('admin.layouts.app')
@section('content')
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Demosession Schedule</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Demosession Schedule</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div><br>
@if(empty($demo->approval) || $demo->approval=="no")
<a href="{{route('visited',$demo->user_id)}}" class="btn btn-primary selectedbutton"> Visited </a>
<a href="{{route('cancelled',$demo->user_id)}}" class="btn btn-danger rejectbutton"> Reject </a>
@include('flash::message')
    <div class="container">
        <div class="m-portlet">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Summary Details
                            </h3>
                        </div>
                    </div>
                </div>        
                <div class="m-portlet__body">
                    <table class="table m-table m-table--head-bg-success">
                        <thead>
                            <tr>                                                                 
                                <th>Demo session Date</th>
                                <th>Reschedule Date</th>                                    
                                <th>Request Date</th>                    
                            </tr>
                        </thead>
                        <tbody>                
                            <tr>
                                <td><a href="" class="btn btn-info" data-toggle="modal" data-target="#m_modal_1"> <i class="fa fa-calendar" aria-hidden="true"> </i> </a></td>
                                @if(empty($demo->verify))
                                    <td> N/A </td>
                                @else                                
                                    <td> {{$demo->reschedule_date}} 
                                        <button type="button" class="btn btn-primary space" data-toggle="modal" data-target="#m_modal_2"> <i class="fa fa-calendar" aria-hidden="true"> </i> </button> 
                                        <a href="{{route('demosession.reject',$demo->user_id)}}" class="btn btn-danger space"> <i class="far fa-trash-alt" aria-hidden="true"> </i> </a> </td>
                                    <td> {{$demo->request_note}} </td>
                                @endif                            
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@elseif($demo->approval=="yes")
    <div class="container">
        <div class="m-portlet">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Demosession Visited !
                            </h3>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
@else
    <div class="container">
        <div class="m-portlet">
            <div class="m-portlet m-portlet--tab">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon m--hide">
                                <i class="la la-gear"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Demosession Cancelled !
                            </h3>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
@endif
{{-- Demo Session Date --}}
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Demosession Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('demodate.post',['id'=>$demo->user_id])}}">
                {{csrf_field()}}
                <div class="modal-body">                    
                    <div class="m-widget1__item">
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Demo Session Date </label>
                                <input type="datetime-local" class="form-control m-input" name="demodate" aria-describedby="emailHelp" required="">
                            </div>                                                                                   
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</td>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- Demo Reschedule Date --}}
<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reschedule Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{route('rescheduledate.post',['id'=>$demo->user_id])}}">
                {{csrf_field()}}
                <div class="modal-body">                    
                    <div class="m-widget1__item">
                        <div class="form-group m-form__group">                                  
                            <div class="col-lg-12">
                                <label for="exampleInputEmail1"> Reschedule Date </label>
                                <input type="datetime-local" class="form-control m-input" name="reschedule_date" aria-describedby="emailHelp" required="">
                            </div>                                                                                   
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Submit</button>    
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</td>
                </div>
            </form>
        </div>
    </div>
</div>
@stop