@extends('admin.layouts.app')
@section('content')
<div class="m-subheader ">
	<div class="d-flex align-items-center">
		<div class="mr-auto">
			<h3 class="m-subheader__title m-subheader__title--separator">Demosession List Panel</h3>
			<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
				<li class="m-nav__item m-nav__item--home">
					<a href="#" class="m-nav__link m-nav__link--icon">
						<i class="m-nav__link-icon la la-home"></i>
					</a>
				</li>
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="" class="m-nav__link">
						<span class="m-nav__link-text">Home</span>
					</a>
				</li>
				<li class="m-nav__separator">-</li>
				<li class="m-nav__item">
					<a href="#" class="m-nav__link">
						<span class="m-nav__link-text">Display Demosession Details </span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="m-content">
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
                        Candidate's List
					</h3>
				</div>
			</div>
		</div>
		<div class="m-portlet__body">		
        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
            <div class="row align-items-center">
                <div class="col-xl-8 order-2 order-xl-1">
                    <div class="form-group m-form__group row align-items-center">							
                        <div class="col-md-4">
                            <div class="m-input-icon m-input-icon--left">
                                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>										
            </div>
        </div>
        <table class="m-datatable" id="html_table" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Stream</th>
                    <th>Reference</th>
                    <th>Previous Demo Attend</th>
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                @foreach($demo as $list)	
                <tr>
                    <td>{{$list->users->name}}</td>
                    <td>{{$list->streams->stream_name}}</td>
                    <td>{{$list->references->reference_name}}</td>
                    <td>{{$list->attend}}</td>                    
                    <td><a href="{{route('demosession.schedule',['user_id'=>$list->user_id])}}" class="btn btn-info"><i class="fa fa-file-photo-o"> View </i> </a> </td>
                </tr>
                @endforeach
            </tbody>
        </table>							
		</div>
	</div>	
</div>
@stop