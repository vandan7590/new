@extends('admin.layouts.app')
@section('content')
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Exam Level</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">ExamLevel Table</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
<div class="m-content">
    <div class="row">
        @if($edit=="null")
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('level.post')}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Exam-Level</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Type</label>
                            <select class="form-control m-input" name="level">                                
                                <option value="laravel">Laravel</option>
                                <option value="php">php</option>
                                <option value="webdesign">Web Design</option>
                                <option value="graphic">Graphic</option>                                
                            </select>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Experience</label>
                            <select class="form-control m-input" name="experience"> 
                                <option value="fresher">Fresher</option>
                                <option value="0-1">0-1</option>
                                <option value="1-2">1-2</option>
                                <option value="2-3">2-3</option>
                                <option value="3-4">3-4</option>
                                <option value="4-5">4-5</option>
                            </select>
                            <span class="text-red error" style="color:red;">{{$errors->first('experience')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">No Of Questions</label>
                            <input type="text" class="form-control m-input" name="no_of_questions">	
                            <span class="text-red error" style="color:red;">{{$errors->first('no_of_questions')}}</span>
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Level Time</label>
                            <input type="text" class="form-control m-input" name="level_time">			            
                            <span class="text-red error" style="color:red;">{{$errors->first('level_time')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>  
        @else
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('editlevel.post',['id'=>$edit->id])}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Edit Level</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Type</label>
                            <select class="form-control m-input" name="level">                                      
                                <option value="laravel">Laravel</option>
                                <option value="php">php</option>
                                <option value="webdesign">Web Design</option>
                                <option value="graphic">Graphic</option> 
                            </select>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Experience</label>
                            <select class="form-control m-input" name="experience"> 
                                <option value="fresher">Fresher</option>
                                <option value="0-1">0-1</option>
                                <option value="1-2">1-2</option>
                                <option value="2-3">2-3</option>
                                <option value="3-4">3-4</option>
                                <option value="4-5">4-5</option>
                            </select>
                            <span class="text-red error" style="color:red;">{{$errors->first('experience')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">No Of Questions</label>
                            <input type="text" class="form-control m-input" name="no_of_questions" value="{{$edit->no_of_questions}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('no_of_questions')}}</span>		                
                        </div>                
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Level Time</label>
                            <input type="text" class="form-control m-input" name="level_time" value="{{$edit->level_time}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('level_time')}}</span>    
                        </div>  
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>
        @endif
        <div class="col-md-12">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
                                Display Job
							</h3>
						</div>
					</div>				
				</div>
				<div class="m-portlet__body">						
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">														
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
											<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
								</div>
							</div>						
						</div>
					</div>
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
								<th title="Field #1">Jobtype</th>
								<th title="Field #2">Experience</th>
								<th title="Field #3">No Of Questions</th>
                                <th title="Field #4">Level Time</th>						
                                <th title="Field #5">Edit</th>
                                <th title="Field #6">Delete</th>												
							</tr>
						</thead>
						<tbody>
							@foreach($level as $job)
							<tr>
								<td>{{$job->level}}</td>
                                <td>{{$job->experience}}</td>
                                <td>{{$job->no_of_questions}}</td>
                                <td>{{$job->level_time}}</td>
                                <td><a href="{{route('level.edit',['id'=>$job->id])}}" class="btn btn-success"><i class="fa fa-edit"></i></a></td>
                                <td><a href="{{route('level.delete',['id'=>$job->id])}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
							</tr>							
							@endforeach			  
						</tbody>
					</table>					
				</div>
			</div>		
		</div>      
    </div>
</div>
@stop

