@extends('admin.layouts.app')
@section('content')
<div class="m-subheader">
    <div class="d-flex align-items-center">
        <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Question Level</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home">
                    <a href="#" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="{{route('dashboard')}}" class="m-nav__link">
                        <span class="m-nav__link-text">Home</span>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                <li class="m-nav__item">
                    <a href="#" class="m-nav__link">
                        <span class="m-nav__link-text">Question Table</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
@include('flash::message')
<div class="m-content">
    <div class="row">
        @if($edit=="null")
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('question.post')}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Add Question</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Level</label>
                            <select class="form-control m-input" name="level_id">
                                @foreach($level as $row)
                                    <option value="{{$row->id}}">{{$row->level}}</option>
                                @endforeach
                            </select>
                        </div>                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Question</label>
                            <input type="text" class="form-control m-input" name="question">
                            <span class="text-red error" style="color:red;">{{$errors->first('question')}}</span> 
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Wrong Answer 1</label>
                            <input type="text" class="form-control m-input" name="option[]">
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Wrong Answer 2</label>
                            <input type="text" class="form-control m-input" name="option[]">
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Wrong Answer 3</label>
                            <input type="text" class="form-control m-input" name="option[]">
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Correct Answer</label>
                            <input type="text" class="form-control m-input" name="option[]">
                        </div> 
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Marks</label>
                            <input type="text" class="form-control m-input" name="marks">	
                            <span class="text-red error" style="color:red;">{{$errors->first('marks')}}</span>
                        </div>
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>  
        @else
        <div class="col-md-12">
            <div class="m-portlet m-portlet--tab">							
                <form class="m-form m-form--fit m-form--label-align-right" method="post" action="{{route('editquestion.post',['id'=>$edit->id])}}">
                    {{csrf_field()}}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group m--margin-top-10">
                            <h3>Edit Question</h3>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleSelect1">Select Level</label>
                            <select class="form-control m-input" name="level_id">
                                @foreach($level as $row)                
                                    <option value="{{$row->id}}">{{$row->level}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Question</label>
                            <input type="text" class="form-control m-input" name="question" value="{{$edit->question}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('question')}}</span>    
                        </div>
                        @foreach($edit->option as $options)
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Option</label>                            
                            <input type="text" value="{{ $options->option }}" name="option[]"class="form-control">
                        </div> 
                        @endforeach                        
                        <div class="form-group m-form__group">
                            <label for="exampleInputEmail1">Marks</label>
                            <input type="text" class="form-control m-input" name="marks" value="{{$edit->marks}}">
                            <span class="text-red error" style="color:red;">{{$errors->first('marks')}}</span>
                        </div>                          
                    </div>                
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </form>                    
            </div>
        </div>
        @endif
        <div class="col-md-12">
			<div class="m-portlet m-portlet--mobile">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
                                Display Question
							</h3>
						</div>
					</div>				
				</div>
				<div class="m-portlet__body">						
					<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
						<div class="row align-items-center">
							<div class="col-xl-8 order-2 order-xl-1">
								<div class="form-group m-form__group row align-items-center">														
									<div class="col-md-4">
										<div class="m-input-icon m-input-icon--left">
											<input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
											<span class="m-input-icon__icon m-input-icon__icon--left">
											<span><i class="la la-search"></i></span>
											</span>
										</div>
									</div>
								</div>
							</div>						
						</div>
					</div>
					<table class="m-datatable" id="html_table" width="100%">
						<thead>
							<tr>
                                <th title="Field #4">Level Name</th>
								<th title="Field #1">Question</th>
								<th title="Field #2">Option</th>
								<th title="Field #3">Mark</th>
                                <th title="Field #5">Edit</th>
                                <th title="Field #6">Delete</th>
							</tr>
						</thead>
						<tbody>
							@foreach($question as $row)
							<tr>
								<td>{{$row->level->level}}</td>
                                <td>{{$row->question}}</td>
                                <td>
                                    <select class="form-control m-input">
                                        @foreach($row->option as $option)
                                            <option>{{$option->option}}</option>
                                        @endforeach
                                    </select>
                                </td>                                
                                <td>{{$row->marks}}</td>
                                <td><a href="{{route('question.edit',['id'=>$row->id])}}" class="btn btn-success"><i class="fa fa-edit"></i></a></td>
                                <td><a href="{{route('question.delete',['id'=>$row->id])}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
							</tr>							
                        </tbody>
							@endforeach			                          
					</table>					
				</div>
			</div>		
		</div>      
    </div>
</div>
@stop

