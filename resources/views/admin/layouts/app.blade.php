<!DOCTYPE html>
<html lang="en">
	@include('admin.layouts.partials.head')

    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			@include('admin.layouts.partials.header')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">    
                @include('admin.layouts.partials.sidebar')
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
                    @yield('content')
				</div>
            </div>
			@include('admin.layouts.partials.footer')
        </div>        
		{{-- <div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
        </div>   --}}        
        <script src="{{asset('admin/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
        <script src="{{asset('admin/assets/app/js/dashboard.js')}}" type="text/javascript"></script>        
    </body>
    <script src="{{asset('admin/assets/demo/default/custom/crud/metronic-datatable/base/html-table.js')}}" type="text/javascript"></script>
    <script src="{{asset('admin/assets/demo/default/custom/crud/forms/widgets/summernote.js')}}" type="text/javascript"></script>
    <script>
        $('#flash-overlay-modal').modal();
    </script>
    <script src="{{asset('admin/assets/demo/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js')}}" type="text/javascript"></script>
    {{-- <script>
        var stateObject = {
        "India": { "Delhi": ["new Delhi", "North Delhi"],
        "Kerala": ["Thiruvananthapuram", "Palakkad"],
        "Goa": ["North Goa", "South Goa"],
        "Gujarat": ["Bhavnagar","Ahmedabad","Rajkot","Surat","Jamnagar","Vadodara"]
        },
        "Australia": {
        "South Australia": ["Dunstan", "Mitchell"],
        "Victoria": ["Altona", "Euroa"]
        }, "Canada": {
        "Alberta": ["Acadia", "Bighorn"],
        "Columbia": ["Washington", ""]
        },
        }
        window.onload = function () {
        var countySel = document.getElementById("countySel"),
        stateSel = document.getElementById("stateSel"),
        districtSel = document.getElementById("districtSel");
        for (var country in stateObject) {
        countySel.options[countySel.options.length] = new Option(country, country);
        }
        countySel.onchange = function () {
        stateSel.length = 1; // remove all options bar first
        districtSel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done
        for (var state in stateObject[this.value]) {
        stateSel.options[stateSel.options.length] = new Option(state, state);
        }
        }
        countySel.onchange(); // reset in case page is reloaded
        stateSel.onchange = function () {
        districtSel.length = 1; // remove all options bar first
        if (this.selectedIndex < 1) return; // done
        var district = stateObject[countySel.value][this.value];
        for (var i = 0; i < district.length; i++) {
        districtSel.options[districtSel.options.length] = new Option(district[i], district[i]);
        }
        }
        }
    </script> --}}
</html>