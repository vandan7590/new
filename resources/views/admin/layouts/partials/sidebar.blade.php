<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
	<!-- BEGIN: Aside Menu -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
		<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
			<li class="m-menu__item {{set_active('dashboard')}}" aria-haspopup="true"><a href="{{route('dashboard')}}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
			<li class="m-menu__item {{set_active('branch')}}"><a href="{{route('branch')}}" class="m-menu__link "><i class="m-menu__link-icon fas fa-code-branch" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Add Branches</span></a></li>
			<li class="m-menu__item {{set_active('jobtype')}}"><a href="{{route('jobtype')}}" class="m-menu__link "><i class="m-menu__link-icon fas fa-briefcase" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Job Add</span></a></li>
			<li class="m-menu__item {{set_active('stream/add')}}"><a href="{{route('stream.add')}}" class="m-menu__link "><i class="m-menu__link-icon fas fa-stream" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Stream Add</span></a></li>
			<li class="m-menu__item {{set_active('reference/add')}}"><a href="{{route('reference.add')}}" class="m-menu__link "><i class="m-menu__link-icon fa fa-id-card" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Reference Add</span></a></li>
			<li class="m-menu__item {{set_active('level/Add')}}"><a href="{{route('level.add')}}" class="m-menu__link "><i class="m-menu__link-icon fa fa-id-card" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Level Add</span></a></li>
			<li class="m-menu__item {{set_active('question/add')}}"><a href="{{route('question.add')}}" class="m-menu__link "><i class="m-menu__link-icon fa fa-id-card" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Question Add</span></a></li>
			<li class="m-menu__item {{set_active('employeeview')}}"><a href="{{route('employeeview')}}" class="m-menu__link "><i class="m-menu__link-icon fa fa-users" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Employee List</span></a></li>
			<li class="m-menu__item {{set_active('trainingview')}}"><a href="{{route('trainingview')}}" class="m-menu__link "><i class="m-menu__link-icon fas fa-graduation-cap" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Training List</span></a></li>
			<li class="m-menu__item {{set_active('demosession/view')}}"><a href="{{route('demosessionview')}}" class="m-menu__link "><i class="m-menu__link-icon fa fa-registered" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Demosession List</span></a></li>
			<li class="m-menu__item {{set_active('demosession/view')}}"><a href="{{route('plan')}}" class="m-menu__link "><i class="m-menu__link-icon fas fa-rupee-sign" style="color:ivory;"><span></span></i><span class="m-menu__link-text">Plan List</span></a></li>			
		</ul>						   
	</div>						
</div>